package week8;

public class Cow extends Animal
{
    public Cow(String name, int id)
    {
        super(name, id);
    }

    @Override
    public void eat()
    {
        System.out.println("我吃草");
    }

    @Override
    public void sleep()
    {
        System.out.println("我趴着睡");
    }

    @Override
    public void introduction()
    {
        System.out.println("我是666号");
    }

    @Override
    public String toString() {
        String a;
        a = "我叫" + name + "，我的编号是" + id + ".";
        return a;
    }
}
