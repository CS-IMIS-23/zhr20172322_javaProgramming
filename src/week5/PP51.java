package week5;

// This is PP5.1.
import java.util.Scanner;

public class PP51
{
    public static void main(String[] args)
    {
        int a;

        Scanner b = new Scanner(System.in);

        System.out.println("Please enter an integer years: ");
        a = b.nextInt();

        if (a < 1582)
        System.out.println("Error! The Gregorian calendar was not used before 1582! ");

        else
        {if (a%4==0&&a%100!=0||a%100==0||a%400==0)
            System.out.println("This year IS a leap year!");
        else
            System.out.println("This year is NOT a leap year!");
        }
    }
}
