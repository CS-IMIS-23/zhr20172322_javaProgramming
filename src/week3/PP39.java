package week3;
import java.util.Random;

public class PP39
{
    public static void main(String[] args) {
        Random abc = new Random();
        int a, b;
        double c, d;
        a = abc.nextInt(20)+1;
        System.out.println("We make a random radius to cylinder: " + a);

        b = abc.nextInt(20)+1;
        System.out.println("We make a random high to cylinder: " + b);

        c = Math.PI*a*a*b;
        System.out.println("The cylinder volume is :" + c);

        d = Math.PI*2*a*b;
        System.out.println("The cylinder surface area: " + d);
    }
}
