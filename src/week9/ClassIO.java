package week9;

import java.io.*;
import java.util.Scanner;

public class ClassIO {
    public static void main(String[] args) throws IOException {
        //  创建文件
        try {
            File file = new File("C:a", "未排序.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            //  将数据写入文件
            String num;
            System.out.print("输入一些整型数，将对他们进行排序：");
            Scanner scan = new Scanner(System.in);
            num = scan.nextLine();
            OutputStream outputStream = new FileOutputStream(file);
            byte[] a = num.getBytes();
            outputStream.write(a);
            //  从文件中读取数据
            InputStream inputStream = new FileInputStream(file);
            byte[] buf = new byte[1024];
            StringBuffer b = new StringBuffer();
            while (inputStream.read(buf) != -1) {
                b.append(new String(buf));
            }

            System.out.println(b);
            //  使用插入排序将数据排序
            InsertionSort.insertionSort(buf);
            StringBuffer d = new StringBuffer();
            d.append(new String(buf));
            System.out.println(d);
            String e = d.toString();
            //  把排序后的数据写入一个新文件
            File file1 = new File("C:a", "已排序.txt");
            if (!file1.exists()) {
                file1.createNewFile();
            }
            Writer writer = new FileWriter(file1);
            writer.write(e);
            writer.flush();
        }
        catch (IOException e)
        {
            System.out.println("文件路径错误，将修改成为一个存在的路径，默认保存在C:\\Users\\机械革命.000\\IdeaProjects\\zhr20172322_javaProgramming");
            File file = new File("C:", "未排序.txt");
            if (!file.exists()) {
                file.createNewFile();
            }
            //  将数据写入文件
            String num;
            System.out.print("输入一些整型数，将对他们进行排序：");
            Scanner scan = new Scanner(System.in);
            num = scan.nextLine();
            OutputStream outputStream = new FileOutputStream(file);
            byte[] a = num.getBytes();
            outputStream.write(a);
            //  从文件中读取数据
            InputStream inputStream = new FileInputStream(file);
            byte[] buf = new byte[1024];
            StringBuffer b = new StringBuffer();
            while (inputStream.read(buf) != -1) {
                b.append(new String(buf));
            }

            System.out.println(b);
            //  使用插入排序将数据排序
            InsertionSort.insertionSort(buf);
            StringBuffer d = new StringBuffer();
            d.append(new String(buf));
            System.out.println(d);
            String F = d.toString();
            //  把排序后的数据写入一个新文件
            File file1 = new File("C:", "已排序.txt");
            if (!file1.exists()) {
               file1.createNewFile();
            }
            Writer writer = new FileWriter(file1);
            writer.write(F);
            writer.flush();
        }
        }
    }