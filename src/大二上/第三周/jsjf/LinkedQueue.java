package 大二上.第三周.jsjf;

import 大二上.第三周.jsjf.exceptions.*;


public class LinkedQueue<T> implements QueueADT<T>
{
    private int count;
    private LinearNode<T> head, tail;
    LinearNode current,temp;


    public LinkedQueue()
    {
        count = 0;
        head = tail = null;
    }

    @Override
    public void enqueue(T element) {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty()) {
            head = node;
        } else {
            tail.setNext(node);
        }

        tail = node;
        count++;
    }

    @Override
    public T dequeue() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }

        T result = head.getElement();
        head = head.getNext();
        count--;

        if (isEmpty()) {
            tail = null;
        }

        return result;
    }

    @Override
    public T first() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }
        T result = head.getElement();

        return result;
    }

    @Override
    public boolean isEmpty()
    {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size()
    {
        return count;
    }

    @Override
    public void addInList(int index, T element) throws Exception {
        if(index <0 ||index >count)
        {
            throw new Exception("参数错误！");
        }
        index(index - 2);

        current.setNext(new LinearNode(element,current.next));

        count ++;
    }

    public void addFisrt(T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);
        current = head;
        node.next = head;
        head = node;
        count++;
    }

    public void delete(int index) throws Exception {

        //判断链表是否为空
        if(isEmpty())
        {
            throw new Exception("链表为空，无法删除！");
        }
        if(index <0 ||index >count)
        {
            throw new Exception("参数错误！");
        }
        index(index - 2);  //  定位到要操作结点的前一个结点对象。

        current.setNext(current.next.next);

        count--;
    }

    public void index(int index) throws Exception
    {
        if(index <-1 || index > count -1)
        {
            throw new Exception("参数错误！");
        }
        if(index == -1)    //因为第一个数据元素结点的下标是0，那么头结点的下标自然就是-1了。
        {
            return;
        }
        current = head.next;
        int j = 0;  //循环变量
           while(current != null&&j<index)
           {
               current = current.next;
               j++;
           }
    }

    public void selectionSort()
    {
        tail = null;
        LinearNode cur = head;
        LinearNode zemme = head;

        LinearNode smallPre = null;
        LinearNode small = null;

        while (cur !=null)
        {
            small = cur;
            smallPre = getSmallestPreNode(cur);
            if (smallPre != null)
            {
                small = smallPre.next;
                smallPre.next = small.next;
            }
            cur = cur == small ? cur.next : cur;
            if(tail==null){
                head=small;  //第一个
            }else {
                tail.next=small;
            }
            tail=small;  //更新下尾部


        }
    }


    public static LinearNode getSmallestPreNode(LinearNode head){

        LinearNode smallPre=null;
        LinearNode small=head;
        LinearNode pre = head;
        LinearNode cur=head.next;

        while(cur!=null){
            if(Integer.parseInt(String.valueOf(cur.element)) <Integer.parseInt(String.valueOf(small.element)))
            {
                smallPre=pre;
                small=cur;
            }
            pre=cur;
            cur=cur.next;
        }
        return smallPre;
    }

    @Override
    public String toString()
    {
        LinearNode node;
        String result="";
        node = head;
        int a = count;
        while (a > 0) {

            result += node.getElement()+" ";

            node = node.getNext();
            a--;
        }
        return result;
    }
}