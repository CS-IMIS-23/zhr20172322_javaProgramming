//***************************************************************************************
// Addition.java                 Author: zhangyeye233
//
//  Demostrates the difference between the addition and string concatenation aperators.
//***************************************************************************************

public class Addition
{
    //-----------------------------------------------------------------------------------
    //  Concatenates and adds two numbers and prints the results.
    //-----------------------------------------------------------------------------------
    public static void main(String[] args)
    {
       System.out.println("21 and 55 concatenated: " + 21 + 55);

       System.out.println("21 and 55 added: " + (21 + 55));
    }
}
