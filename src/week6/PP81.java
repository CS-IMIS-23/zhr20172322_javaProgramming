package week6;

// This PP8.1.
import java.util.Scanner;

public class PP81 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int a;
        int[] b = new int[51];

        while (1>-1)
        {
            System.out.println("Please enter a integer (0 to 50 , else to quit): ");
            a = scan.nextInt();
            if (a >= 0 && a <= 50)
                b[a]++;
            else
                break;
        }
        for (int d = 0;d < b.length; d++)
        {
            System.out.println("The " + d + " have appeared " + b[d] + " times");
        }
    }
}
