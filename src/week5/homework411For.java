package week5;

// This homework with "for"

import java.util.Scanner;

public class homework411For
{
    public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int a,b,n;

        System.out.println("Please enter a integer(n) :");
        n = scan.nextInt();
        b = n;

        for (a=1;a<n;a++)
            b *=a;

        System.out.println("n! = " + b);
    }
}
