package week12;

import 四则运算.Calculator;
import 实验三.KeyAgree;
import 实验三.Key_DH;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;

public class Server1 {
    public static void main(String[] args) {
        Calculator cC = new Calculator();
        ServerSocket serverForClient = null;
        Socket socketOnServer = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            serverForClient = new ServerSocket(1013);
        } catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待客户连接");
            //  启动服务器，客户连接后进行下一步
            socketOnServer = serverForClient.accept();
            System.out.println("客户已连接");
            out = new DataOutputStream(socketOnServer.getOutputStream());
            in = new DataInputStream(socketOnServer.getInputStream());

            //  利用DH算法进行3DES或AES算法的密钥交换
            Key_DH.DH("Bpub.dat","Bpri.dat");
            int len = Integer.parseInt(in.readUTF());
            byte[] np = new byte[len];

            int t = 0;
            while (t<len) {
                String temp = in.readUTF();
                np[t] = Byte.parseByte(temp);
                t ++;
            }
            ObjectInputStream ois = new ObjectInputStream (new ByteArrayInputStream (np));
            Key k2 = (Key)ois.readObject();;
            FileOutputStream f2 = new FileOutputStream("Apub.dat");
            ObjectOutputStream b2 = new ObjectOutputStream(f2);
            b2.writeObject(k2);

            FileInputStream fp = new FileInputStream("Bpub.dat");
            ObjectInputStream bp = new ObjectInputStream(fp);
            Key kp = (Key) bp.readObject();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(kp);
            byte[] kb = baos.toByteArray();
            out.writeUTF(kb.length + "");

            int i = 0;
            while ( i < kb.length) {
                out.writeUTF(kb[i] + "");
                i ++;
            }
            KeyAgree.DH("Apub.dat","Bpri.dat");

            // 读取信息
            String leng = in.readUTF();
            byte[] ctext = new byte[Integer.parseInt(leng)];

            int w = 0;
            while (w<Integer.parseInt(leng))
            {
                String temp = in.readUTF();
                ctext[w] = Byte.parseByte(temp);
                w ++;
            }
            // 获取密钥
            FileInputStream f = new FileInputStream("sb.dat");
            byte[] keysb = new byte[24];
            f.read(keysb);
            System.out.println("公共密钥是：");

            int r = 0;
            while (r<24)
            {
                System.out.print(keysb[r]+",");
                r ++;
            }
            System.out.println("");
            SecretKeySpec k = new SecretKeySpec(keysb, "DESede");
            // 解密
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);
            System.out.println("");
            // 显示明文
            String p = new String(ptext,"UTF8");
            System.out.println("解密后需要计算的后缀表达式：" + p);
            System.out.println("结果为：" + cC.evaluate(p));
            out.writeUTF(cC.evaluate(p)+"");
        } catch (Exception e) {
            System.out.println("客户已断开" + e);
        }
    }
}
