package week9.PP项目;

import java.util.Scanner;

public class PP12_9 {

    public static void main(String[] args) {
        int lines;
        System.out.println("请输入行数（大于0）：");
        Scanner input=new Scanner(System.in);
        lines=input.nextInt();
        while (lines <= 0)
        {
            System.out.println("错误，请输入有效数字！");
            System.out.println("请输入行数（大于0）：");
            Scanner a = new Scanner(System.in);
            lines=a.nextInt();
        }
        for(int i=1;i<=lines;i++){
            for(int k=1;k<lines-i+1;k++){
                System.out.print("\t");
            }
            for(int j=1;j<=i;j++){
                System.out.print("\t" + fun(i,j)+"\t");
            }
            System.out.println();
        }
    }
    static int fun(int n,int k) //  递归实现
                                  //   n对应行，k对应列
    {
        if(k==1||n==k)
            return 1;
        else
            return fun(n-1,k-1)+fun(n-1,k);
    }
}