package 实验三;

  //        这是20172303范雯琪同学的Complex代码，使用Alibaba代码规范规范了她的代码
/*  @author zhangyeye233
  @date 2018/5/16*/
public class Complex
{
    public double realPart;
    public double imagePart;

    public double getRealPart()
    {
        return realPart;
    }
    public void setRealPart(double real)
    {
        realPart = real;
    }

    public double getImagePart()
    {
        return imagePart;
    }
    public void setImagePart(double image)
    {
        imagePart = image;
    }

    public Complex() {}
    public Complex(double r,double i){
        realPart =r;
        imagePart =i;
    }

    @Override
    public boolean equals(Object obj)
    {
        if(this==obj) {
            return true;
        } else if(obj==null) {
            return false;
        } else
        {
            double r = ((Complex) obj).getRealPart();
            double i = ((Complex) obj).getImagePart();
            if((r== realPart)&&(i== imagePart))
            {
                return true;
            }
            else {
                return false;
            }
        }
    }

    @Override
    public String toString()
    {
        return realPart +"+"+ imagePart +'i';
    }
    public Complex complexAdd(Complex a)
    {
        Complex com = new Complex();
        com.imagePart =this.imagePart +a.imagePart;
        com.realPart =this.realPart +a.realPart;
        return com;
    }
    public Complex complexSub(Complex b){
        Complex com = new Complex();
        com.imagePart =this.imagePart -b.imagePart;
        com.realPart =this.realPart -b.realPart;
        return com;
    }
    public Complex complexMulti(Complex c){
        Complex com = new Complex();
        com.realPart =this.realPart *c.realPart -this.imagePart *c.imagePart;
        com.imagePart =this.realPart *c.imagePart +this.imagePart *c.realPart;
        return com;
    }
    public Complex complexDiv(Complex d){
        Complex com = new Complex();
        Complex b=new Complex(d.realPart,-d.imagePart);
        System.out.println(b.toString());
        com.realPart =(this.complexMulti(b).realPart)/(d.realPart *d.realPart +d.imagePart *d.imagePart);
        com.imagePart =(this.complexMulti(b).imagePart)/(d.realPart *d.realPart +d.imagePart *d.imagePart);
        System.out.println(com.imagePart);
        return com;
    }
}
