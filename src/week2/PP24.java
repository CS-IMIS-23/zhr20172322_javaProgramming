//  This is PP2.4

import java.util.Scanner;

public class PP24
{
    public static void main(String[] args)

    {
       String name;
       String college;
       String petname;

       int age;
       Scanner scan = new Scanner(System.in);
    
       System.out.println("Please enter your name: ");
       name = scan.nextLine();

       System.out.println("Please enter your age: ");
       age = scan.nextInt();

       System.out.println("Please enter your college: ");
       college = scan.next();

       System.out.println("Please enter your petname: ");
       petname = scan.next();

       System.out.println("Hello, my is " + name + " and I am " + age + " years");
       System.out.println("old. I'm enjoying my time at " + college + ", though");
       System.out.println("I miss my pet " + petname + " very much!");
    }
}
