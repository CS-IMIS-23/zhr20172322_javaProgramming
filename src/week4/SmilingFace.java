package week4;

//*************************************************************************************************
//  SmilingFace.java     Author: zhangyeye233
//
//  Demonstrates the use of a separate panel class.
//*************************************************************************************************

import javax.swing.JFrame;

public class SmilingFace
{
    //---------------------------------------------------------------------------------------------
    //  Creates the main frame of the program.
    //---------------------------------------------------------------------------------------------
    public static void main(String[] args)
    {
        JFrame frame = new JFrame("Smiling Face");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        SmilingFacePane1 panel = new SmilingFacePane1();

        frame.getContentPane().add(panel);

        frame.pack();
        frame.setVisible(true);
    }
}
