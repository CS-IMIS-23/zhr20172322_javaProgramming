package week10.PP项目;

public class DVDList {
    private DVDNode list;

    public DVDList() {
        list = null;
    }

    public void add(DVD dvd) {
        DVDNode node = new DVDNode(dvd);
        DVDNode current;

        if (list == null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }

    @Override
    public String toString() {
        String result = "";

        DVDNode current = list;

        while (current != null) {
            result += current.dvd + "\n";
            current = current.next;
        }

        return result;
    }

    public class DVDNode {
        public DVD dvd;
        public DVDNode next;

        public DVDNode(DVD dv) {
            dvd = dv;
            next = null;
        }
    }
}
