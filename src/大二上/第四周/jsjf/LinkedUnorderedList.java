package 大二上.第四周.jsjf;

/**
 * LinkedUnorderedList represents a singly linked implementation of an 
 * unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedUnorderedList<T> extends LinkedList<T> 
         implements UnorderedListADT<T>
{

    public LinkedUnorderedList()
    {
        super();
    }


    @Override
    public void addToFront(T element)
    {
        LinearNode<T> node = new LinearNode<T>((T) element);
        if (head == null) {
            head = node;
            tail = node;
        }
        else {
            node.setNext(head);
            head = node;
        }
        count++;
    }
	

    @Override
    public void addToRear(T element) {
        LinearNode last = head;
        for (int i = 0; i < count - 1; i++)
        {
            last = last.getNext();
        }
        LinearNode node = new LinearNode(element);
        last.setNext(node);
        count++;
    }
	

    @Override
    public void addAfter(T element, T target)
    {
        LinearNode<T> node = new LinearNode<>(element);
        LinearNode<T> current = head;
        LinearNode<T> temp = current.getNext();

        for (int i = 0;i < count - 1;i++){
            if (target.equals(current.getElement())) {
                current.setNext(node);
                node.setNext(temp);
                count++;
                return;
            }
            current = temp;
            temp = temp.getNext();
        }
    }
}
