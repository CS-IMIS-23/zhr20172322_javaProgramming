package week10.五月十四日课上测试;//********************************************************************
//  Magazine.java       Author: Lewis/Loftus
//
//  Represents a single magazine.
//********************************************************************

import java.util.Objects;

public class Magazine
{
   private String title;

   //-----------------------------------------------------------------
   //  Sets up the new magazine with its title.
   //-----------------------------------------------------------------
   public Magazine(String newTitle)
   {    
      title = newTitle;
   }

   @Override
   public boolean equals(Object obj)     //   重写equals方法
   {
      Magazine obj1 = (Magazine) obj;

      if(this.title.equals(obj1.title))    //  此equals方法仅判断title是否相等，而非之前equals方法判断地址
          return true;
      else
          return false;
   }

   //-----------------------------------------------------------------
   //  Returns this magazine as a string.
   //-----------------------------------------------------------------
   public String toString()
   {
      return title;
   }
}
