package 大二上.第六周;

import java.io.FileNotFoundException;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 */
public class BackPainAnalyzer1
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("So, you're having back pain.");

        DecisionTree expert = new DecisionTree("input.txt");
        expert.RecursionLevelOrder(expert.getTree().root);




        System.out.println();
        expert.UnrecursionLevelOrder();
    }
}
