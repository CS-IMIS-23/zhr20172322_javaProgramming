package week7;

public class PictureBook extends ReadingMaterials
{
    String pictureAbout;
    public void setPictureAbout(String PI)
    {
        pictureAbout = PI;
    }

    public String getPictureAbout()
    {
        return pictureAbout;
    }

    @Override
    public String toString() {
        String a;
        a = "图书的名字是" + names + ",它的图片是关于" + pictureAbout + "，它的作者是" + AUTHOR + "，它的关键词有"
                + Keys + "。";
        return a;
    }
}
