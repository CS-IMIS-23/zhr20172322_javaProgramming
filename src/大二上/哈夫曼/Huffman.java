package 大二上.哈夫曼;

import java.io.*;
import java.util.*;

public class Huffman {

    public static void main(String[] args) throws IOException {
        List<HuffmanNode<String>> list = new ArrayList<>();
        List<HuffmanNode<String>> list2;
        List<String> list3 = new ArrayList<>();
        List<String> list4 = new ArrayList<>();
        List<String> list5 = new ArrayList<>();
        String temp2 = "",temp3 = "";
        String result="";
        double num2 = 0;


        File file = new File("C:\\Users\\机械革命.000\\Desktop\\程序设计\\大二上\\哈夫曼\\input.txt");
        ReadTxt read = new ReadTxt();
        String temp = read.txtString(file);
        System.out.println("读取的文件是：" + temp);
        int[] num = read.getNumber();  //  存放出现次数的数组
        char[] chars = read.getChars();  //  存放元素的数组
        for(int i = 0;i<27;i++){
            System.out.print(chars[i]+"："+num[i]+"   ");
            list.add(new HuffmanNode<>(chars[i]+"",num[i]));
        }
        Collections.sort(list);  //  按照自然顺序排序

        System.out.println();
        HuffmanCoding huffmanTree = new HuffmanCoding();
        HuffmanNode<String> root = huffmanTree.createTree(list);

        list2 = huffmanTree.BFS(root);//  利用广度优先遍历遍历整棵树后赋值
        for(int i = 0;i<list2.size();i++){
            if(list2.get(i).getData()!=null) {
                list3.add(list2.get(i).getData());
                list4.add(list2.get(i).getCode());
            }
        }


        for(int i = 0;i<list2.size();i++){
            num2 += list2.get(i).getWeight();
        }

        Collections.sort(list3);
        System.out.println("出现的概率中第一个代表空格。");
        for(int i = 0;i<list3.size();i++){
            System.out.print(list3.get(i) + "出现的概率为：" + list2.get(i).getWeight()/num2 + "  \n");
        }




        System.out.println();

        for(int i = 0;i<list4.size();i++){
            System.out.print(list3.get(i)+"的编码为："+list4.get(i)+" \n");
        }
        System.out.println();

        for(int i = 0;i<temp.length();i++){
            for(int j = 0;j<list3.size();j++){
                if(temp.charAt(i) == list3.get(j).charAt(0)) {
                    result += list4.get(j);
                }
            }
        }





        for(int i = 0;i<result.length();i++){
            list5.add(result.charAt(i)+"");
        }


        while (list5.size()>0){
            temp2 = temp2+"" +list5.get(0);
            list5.remove(0);
            for(int i=0;i<list4.size();i++){
                if (temp2.equals(list4.get(i))) {
                    temp3 = temp3+""+list3.get(i);
                    temp2 = "";
                }
            }
        }
        System.out.println();

        System.out.println("编码前：" + temp);
        System.out.println("编码后为：\n"+ result);
        System.out.println("解码后："+"\n"+temp3);

        File file2 =new File("C:\\Users\\机械革命.000\\Desktop\\程序设计\\大二上\\哈夫曼\\output.txt");
        Writer out =new FileWriter(file2);
        out.write(result);  //  将哈夫曼编码放入output.txt中
        out.close();
    }
}
