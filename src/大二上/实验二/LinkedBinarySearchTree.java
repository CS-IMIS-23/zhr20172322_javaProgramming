package 大二上.实验二;


import 大二上.第七周.jsjf.BinarySearchTreeADT;
import 大二上.第七周.jsjf.exceptions.ElementNotFoundException;
import 大二上.第七周.jsjf.exceptions.EmptyCollectionException;
import 大二上.第七周.jsjf.exceptions.NonComparableElementException;
import 大二上.第六周.jsjf.BinaryTreeNode;
import 大二上.第六周.jsjf.LinkedBinaryTree;

/**
 * LinkedBinarySearchTree implements the BinarySearchTreeADT interface 
 * with links.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedBinarySearchTree<T> extends LinkedBinaryTree<T>
                                        implements BinarySearchTreeADT<T>
{
    public LinkedBinarySearchTree() 
    {
        super();
    }

    public LinkedBinarySearchTree(T element) 
    {
        super(element);
        
        if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("LinkedBinarySearchTree");
        }
    }
    @Override
    public void addElement(T element)
    {
        if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("LinkedBinarySearchTree");
        }

        Comparable<T> comparableElement = (Comparable<T>)element;

        if (isEmpty()) {
            root = new BinaryTreeNode<T>(element);
        } else
        {
            if (comparableElement.compareTo(root.getElement()) < 0)
            {
                if (root.getLeft() == null) {
                    this.getRootNode().setLeft(new BinaryTreeNode<T>(element));
                } else {
                    addElement(element, root.getLeft());
                }
            }
            else
            {
                if (root.getRight() == null) {
                    this.getRootNode().setRight(new BinaryTreeNode<T>(element));
                } else {
                    addElement(element, root.getRight());
                }
            }
        }
        modCount++;
    }

    private void addElement(T element, BinaryTreeNode<T> node) 
    {
        Comparable<T> comparableElement = (Comparable<T>)element;
        
        if (comparableElement.compareTo(node.getElement()) < 0)
        {
            if (node.getLeft() == null) {
                node.setLeft(new BinaryTreeNode<T>(element));
            } else {
                addElement(element, node.getLeft());
            }
        }
        else
        {
            if (node.getRight() == null) {
                node.setRight(new BinaryTreeNode<T>(element));
            } else {
                addElement(element, node.getRight());
            }
        }
    }

    @Override
    public T removeElement(T targetElement)
                                  throws ElementNotFoundException
    {
        T result = null;

        if (isEmpty()) {
            throw new ElementNotFoundException("LinkedBinarySearchTree");
        } else
        {
            BinaryTreeNode<T> parent = null;
            if (((Comparable<T>)targetElement).equals(root.element)) 
            {
                result =  root.element;
                BinaryTreeNode<T> temp = replacement(root);
                if (temp == null) {
                    root = null;
                } else
                {
                    root.element = temp.element;
                    root.setRight(temp.right);
                    root.setLeft(temp.left);
                }

                modCount--;
            }
            else 
            {                
                parent = root;
                if (((Comparable)targetElement).compareTo(root.element) < 0) {
                    result = removeElement(targetElement, root.getLeft(), parent);
                } else {
                    result = removeElement(targetElement, root.getRight(), parent);
                }
            }
        }
        
        return result;
    }
    private T removeElement(T targetElement, BinaryTreeNode<T> node, BinaryTreeNode<T> parent)
    throws ElementNotFoundException 
    {
        T result = null;
        
        if (node == null) {
            throw new ElementNotFoundException("LinkedBinarySearchTree");
        } else
        {
            if (((Comparable<T>)targetElement).equals(node.element)) 
            {
                result =  node.element;
                BinaryTreeNode<T> temp = replacement(node);
                if (parent.right == node) {
                    parent.right = temp;
                } else {
                    parent.left = temp;
                }

                modCount--;
            }
            else 
            {                
                parent = node;
                if (((Comparable)targetElement).compareTo(node.element) < 0) {
                    result = removeElement(targetElement, node.getLeft(), parent);
                } else {
                    result = removeElement(targetElement, node.getRight(), parent);
                }
            }
        }
        
        return result;
    }

    private BinaryTreeNode<T> replacement(BinaryTreeNode<T> node) 
    {
        BinaryTreeNode<T> result = null;
        
        if ((node.left == null) && (node.right == null)) {
            result = null;
        } else if ((node.left != null) && (node.right == null)) {
            result = node.left;
        } else if ((node.left == null) && (node.right != null)) {
            result = node.right;
        } else
        {
            BinaryTreeNode<T> current = node.right;
            BinaryTreeNode<T> parent = node;
            
            while (current.left != null)
            {
                parent = current;
                current = current.left;
            }
            
            current.left = node.left;
            if (node.right != current)
            {
                parent.left = current.right;
                current.right = node.right;
            }
            
            result = current;
        }
        
        return result;
    }

    @Override
    public void removeAllOccurrences(T targetElement)
                   throws ElementNotFoundException 
    {
        removeElement(targetElement);
        
        try
        {
            while (contains((T)targetElement)) {
                removeElement(targetElement);
            }
        }
        
        catch (Exception ElementNotFoundException)
        {
        }
    }

    @Override
    public T removeMin() throws EmptyCollectionException
    {
        T result = null;

        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedBinarySearchTree");
        } else
        {
            if (root.left == null) 
            {
                result = root.element;
                root = root.right;
            }
            else 
            {
                BinaryTreeNode<T> parent = root;
                BinaryTreeNode<T> current = root.left;
                while (current.left != null) 
                {
                    parent = current;
                    current = current.left;
                }
                result =  current.element;
                parent.left = current.right;
            }

            modCount--;
        }
 
        return result;
    }

    @Override
    public T removeMax() throws EmptyCollectionException
    {
        T result = null;

        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedBinarySearchTree");
        } else
        {
            //  如果根结点的右子树是空，则直接移除根结点，并且将根结点的左孩子作为新的根结点
            if (root.right == null)
            {
                result = root.element;
                root = root.left;
            }
            //  如果根结点的右子树不为空，则在右子树中查找最大值
            else
            {
                BinaryTreeNode<T> parent = root;
                BinaryTreeNode<T> current = root.right;
                while (current.right != null)
                {
                    parent = current;
                    current = current.right;
                }
                result =  current.element;
                parent.right = current.left;
            }
        }

        return result;
    }

    @Override
    public T findMin() throws EmptyCollectionException
    {
        T result;
        //  如果右子树为空则直接返回根结点的值
        if(root.left == null){
            return root.element;
        }
        BinaryTreeNode<T> current = root.left;
        //  如果右子树不为空，则找到最小的左叶节点
        while (current.left != null)
        {
            current = current.left;
        }
        result =  current.element;
        return result;
    }

    @Override
    public T findMax() throws EmptyCollectionException
    {
        T result;
        //  与查找最小的同理
        if (root.right==null){
            return root.right.element;
        }
        BinaryTreeNode<T> current = root.right;
        while (current.right != null)
        {

            current = current.right;
        }
        result =  current.element;
        return result;
    }

    @Override
    public T find(T targetElement) throws ElementNotFoundException
    {
        BinaryTreeNode<T> current = findNode(targetElement,root);
        if (current == null) {
            throw new ElementNotFoundException("LinkedBinaryTree");
        }

        return (current.getElement());
    }

    @Override
    public LinkedBinarySearchTree<T> getLeft()
    {
        if(root == null) {
            throw new EmptyCollectionException("Get left operation failed. The tree is empty.");
        }


        LinkedBinarySearchTree<T> result = new LinkedBinarySearchTree<>();
        result.root = root.getLeft();
        return result;
    }

    @Override
    public LinkedBinarySearchTree<T> getRight()
    {
        if(root == null) {
            throw new EmptyCollectionException("Get right operation failed. The tree is empty.");
        }

        LinkedBinarySearchTree<T> result = new LinkedBinarySearchTree<>();
        result.root = root.getRight();
        return result;
    }

    private BinaryTreeNode<T> findNode(T targetElement, BinaryTreeNode<T> next) 
    {
        if (next == null) {
            return null;
        }
        if (next.getElement().equals(targetElement))
        {
            return next;
        }
        BinaryTreeNode<T> temp = findNode(targetElement,next.getLeft());

        if (temp == null)
        {
            temp = findNode(targetElement,next.getRight());
        }

        return temp;

    }
}

