package 大二上.第二周.蓝墨云;

import java.util.ArrayList;
import java.util.Scanner;

public class Numtest
{
    public static void main(String[] args) {
        int number, i = 0;
        String anwser = "y";

        ArrayList arrayList = new ArrayList();

        // 交互，由用户决定是否继续输入

        while (anwser.equalsIgnoreCase("y")){
            System.out.print("Enter a number: ");
            Scanner scanner = new Scanner(System.in);
            number = scanner.nextInt();
            arrayList.add(number);
            i++;
            System.out.print("Do you want to enter again? (y/n)");
            anwser = scanner.next();
        }


        Num head = new Num((Integer) arrayList.get(0));
        Num node = head;

        for (int b = 1;b < arrayList.size();b++){
            Num temp = new Num((Integer) arrayList.get(b));// 增添一个temp指针,防止node.next为空
            node.next = temp;// 防止产生NullPointerException
            node = temp;
        }


        //   测试
        NumList.Print(head);//  先输出之前输入的数值，按照输入的顺序
        System.out.println();

        // 插入测试
        Num number1 = new Num(233);
        Num number2 = new Num(123);
        Num number3 = new Num(666);
        NumList.Insert(head,number1);//结尾插入
        NumList.Insert(2,head,number2);//中间插入
        NumList.preInsert(head,number3);//头部插入

        NumList.Print(head);
        System.out.println();
        //  删除测试
        NumList.Delete(2,head);
        NumList.Print(head);
        System.out.println();
        //   排序
        NumList.Sort(head);
        NumList.Print(head);
    }
}
