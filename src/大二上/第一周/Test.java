package 大二上.第一周;

public class Test {
    public static void main(String[] args) {
        ArrayStack a = new ArrayStack();
        a.push("a");
        a.push("b");
        a.push("c");
        System.out.println(a.size());
        System.out.println(a.isEmpty());
        System.out.println(a.pop());
        System.out.println(a.peek());
        System.out.println(a.size());
        System.out.println(a.toString());
    }
}
