package week5;

// This is PP6.7 part2.

public class PP67Stars2
{
    public static void main(String[] args) {
        final int MAX_ROWS = 10;

        for (int row = 1; row <= MAX_ROWS; row++)
        {
            for (int kongge = 9; kongge >= row; kongge--)
                System.out.print(" ");
            for (int stars = 1; stars <= row;stars++)
                System.out.print("*");

            System.out.println();
        }
    }
}
