package week9.PP项目;

import java.util.Scanner;

public class PP11_2 {
    public static void main(String[] args) throws StringTooLongException {
        final int MAX = 20;
        String i = "";

        Scanner scan = new Scanner(System.in);

        while (true) {
            StringTooLongException pr = new StringTooLongException("输入的字符串过长！");

            System.out.println("输入一串字符串，我们将会保存你之前写入的字符串（最好别超过20个字符，不然程序就有可能结束啦。输入DONE退出程序）：");
            String value = scan.next();
            if (value.equals("DONE"))
                break;
            i += value;

            try {
                if (value.length() > MAX)
                    throw pr;
                else
                    System.out.println("你输入的字符串是：" + value);
            }
            catch (StringTooLongException e) {
                System.out.println("虽然你输入的字符串有点长(大于了20个字符)，但是我们还是给你输出了：\n" + value);
            }
        }
    }
}
