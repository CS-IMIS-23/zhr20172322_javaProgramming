package week7;

//*************************************************************************************************
//  Coin.java                     Author: zhangyeye233
//
//  Represents a coin with two sides that can be flipped.
//*************************************************************************************************

public class Coin
{
    private final int HEADS = 0;
    private final int TAILS = 1;

    protected int face;

    //---------------------------------------------------------------------------------------------
    //  Sets up the coin by flipping it initially.
    //---------------------------------------------------------------------------------------------

    public Coin()
    {
        flip();
    }

    //---------------------------------------------------------------------------------------------
    //  Flips the con by randomly choosing a face value.
    //---------------------------------------------------------------------------------------------
    public void flip()
    {
        face = (int) (Math.random() * 2);
    }

    //---------------------------------------------------------------------------------------------
    //  Returns true if the current face of the coin is heads.
    //---------------------------------------------------------------------------------------------
    public boolean isHeads()
    {
        return (face == HEADS);
    }

    //---------------------------------------------------------------------------------------------
    //  Returns the current face of the coin as a string.
    //---------------------------------------------------------------------------------------------
    public String toString()
    {
        String faceName;
        if(face == HEADS)
            faceName = "正";
        else
            faceName = "反";

        return faceName;
    }
}
