package 大二上.第二周;

import 大二上.第一周.EmptyCollectionException;
import 大二上.第一周.StackADT;

public class LinkedStack<T> implements StackADT<T>    //    T是泛型
{
    private int count;
    private LinearNode<T> top;

    public LinkedStack() {
        count = 0;
        top = null;
    }

    @Override
    public void push(T element) {
        LinearNode<T> temp = new LinearNode<T>(element);

        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public T pop() throws EmptyCollectionException {

        if (isEmpty()) {
            throw new EmptyCollectionException("the stack is empty!");
        }

        T result = top.getElement();

        top = top.getNext();

        count--;

        return result;
    }

    @Override
    public T peek() {

        if (isEmpty()) {
            throw new EmptyCollectionException("the stack is empty!");
        }
        T result = top.getElement();


        return result;
    }

    @Override
    public int size() {

        return count;
    }

    @Override
    public boolean isEmpty() {

        boolean p;
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString(){
        LinearNode node;
        String result="";
        node = top;
        int a = count;
        while (a > 0) {

            result += node.getElement()+ " ";

            node = node.getNext();
            a--;

        }
        return result;
    }
}