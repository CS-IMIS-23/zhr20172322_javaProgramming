package 大二上.第五周;

public class LinearNode<T>
{
    public LinearNode<T> next;
    protected T element;

    public LinearNode()
    {
        next = null;
        element = null;
	}

    public LinearNode(T elem)
    {
        next = null;
        element = elem;
    }
    public LinearNode(T element, LinearNode linearNode)
    {
        this.element = element;
        this.next = linearNode;

    }

    public LinearNode<T> getNext()
    {
        return next;
    }

    public void setNext(LinearNode<T> node)
    {
        next = node;
    }

    public T getElement()
    {
        return element;
    }

    public void setElement(T elem)
    {
        element = elem;
    }
}
