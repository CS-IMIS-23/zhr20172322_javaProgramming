package 大二上.第一周;

import java.util.Arrays;

public class ArrayStack<T> implements StackADT<T>

{
    private final int DEFAULT_CAPACITY = 100;
    private final int INCREMENT=10;

    private int top;
    private T[] stack;


    @SuppressWarnings("unchecked")

    public ArrayStack(){

        //stack = newT[DEFAULT_CAPCITY];

        stack = (T[])(new Object[DEFAULT_CAPACITY]);

        top = 0;

    }
//    public ArrayStack(int initialCapacity) {
//        top = 0;
//        stack = (T[]) (new Object[initialCapacity]);
//    }

    @Override
    public void push(T element) {


            if(stack.length == top)

            {
                expandCapacity();
            }

            stack[top] = element;

            top ++;
    }
    private void expandCapacity() {

        stack = Arrays.copyOf(stack, stack.length+INCREMENT);

    }

    @Override
    public T pop() {
        if(isEmpty())
        {
            throw new EmptyCollectionException("the stack is empty!");
        }
        top--;

        T result = stack[top];

        stack[top] = null;

        return result;
    }

    @Override
    public T peek() {
        if(isEmpty())
        {
            throw new EmptyCollectionException("the stack is empty!");
        }
        return stack[top-1];
    }

    @Override
    public boolean isEmpty() {
        if(0== size())

        {
            return true;
        } else

        {
            return false;
        }
    }

    @Override
    public int size() {
        int b = 0;
        for (int a = 0; stack[a] != null; a++)
        {
            b++;
        }
        return b;
    }

    @Override
    public String toString() {
        String r = "";

        for (int g = 0; g < size();g++)
        {
            r += String.valueOf(stack[g] + " ");
        }
        return r;
    }
}


