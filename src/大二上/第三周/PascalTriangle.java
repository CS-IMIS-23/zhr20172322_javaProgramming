package 大二上.第三周;

import 大二上.第三周.jsjf.CircularArrayQueue;

import java.util.Scanner;

public class PascalTriangle {
    public static void main(String[] args) {
        CircularArrayQueue queue = new CircularArrayQueue();
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入要显示的杨辉（帕斯卡）三角的行数：");
        int n = scanner.nextInt();

        queue.enqueue(0);
        queue.enqueue(1);

        System.out.println(1);

        for (int i = 0; i < n - 1; i++){
            queue.enqueue(0);  //  初始值
            for (int j = 1;j <= queue.size() - 1;j++){
                int a = (int) queue.first();
                queue.dequeue();    //  删除
                int b = (int) queue.first();
                int c = a + b;
                queue.enqueue(c);  //  上面临近两个数之和
                System.out.print(c + " ");
            }
            System.out.println();
        }
    }
}