package week10.PP项目;

public class SelectionSortList
{
    private SelectionSortNode list;

    public SelectionSortList()
    {
        list = null;
    }

    private class SelectionSortNode
    {
        int IT;
        public SelectionSortNode next;

        public SelectionSortNode(int it)
        {
            IT = it;
            next = null;
        }
    }

    public void add(int num)   //    增加数
    {
        SelectionSortNode node = new SelectionSortNode(num);
        SelectionSortNode current;

        if (list==null)
            list = node;
        else {
            current = list;
            while (current.next != null)
                current = current.next;
            current.next = node;
        }
    }
    public void selectionsort(){
        SelectionSortNode min;  //   寻找最小值
        SelectionSortNode current;   //   指针
        int temp;   //    一个缓冲区
        SelectionSortNode pre = list;    //   第二个指针
        while (pre != null)
        {
            min = pre;     //   先将首个数看做最小的
            current = min.next;
            while (current != null)    //    比较第二和第一个数的大小，并且如果小于则将第二个数作为最小，如此往复
            {
                if (current.IT >= min.IT)
                {
                    current = current.next;
                }
                else if (current.IT < min.IT)
                {
                    min = current;
                    current = current.next;
                }
            }
            temp = min.IT;      //    利用temp缓存实现值得转换
            min.IT = pre.IT;
            pre.IT = temp;
            pre = pre.next;
        }
    }

    @Override
    public String toString() {
        String result = "";
        SelectionSortNode current = list;
        while (current != null){
            result += current.IT + "\n";
            current = current.next;
        }
        return result;
    }
}
