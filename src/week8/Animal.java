package week8;

public abstract class Animal
{
    protected String name;
    protected int id;

    public Animal(String name, int id)
    {
        this.name = name;
        this.id = id;
    }

    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction() ;
}
