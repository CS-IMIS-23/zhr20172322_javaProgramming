package 大二上.实验三.cn.edu.besti.cs1723.zhr2322;

import junit.framework.TestCase;

public class SearchingTest<T> extends TestCase {


    @org.junit.Test
    public void testLinearSearch() throws Exception {
        Comparable[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1111, 1212, 1999, 2000,2322};//正序
        assertEquals(true, Searching.linearSearch(data, 0, 14, 7));
        assertEquals(true, Searching.linearSearch(data, 0, 14, 3));
        assertEquals(false, Searching.linearSearch(data, 0, 2, 10));
        assertEquals(true, Searching.linearSearch(data, 0, 14, 2322));//边界

        Comparable[] data1 = {2322,5,4,3,2,1};//逆序
        assertEquals(true, Searching.linearSearch(data1,0,5,2));
        assertEquals(true, Searching.linearSearch(data1,0,5,1));//边界
        assertEquals(true, Searching.linearSearch(data1,0,5,2322));//边界
        assertEquals(false, Searching.linearSearch(data1,0,5,13333));

        Comparable[] data2 = {"A","B","C","D","E"};
        assertEquals(true, Searching.linearSearch(data2,0,4,"C"));
        assertEquals(true, Searching.linearSearch(data2,0,4,"E"));
        assertEquals(true, Searching.linearSearch(data2,0,4,"A"));
        assertEquals(false, Searching.linearSearch(data2,0,4,"T"));
    }

//    public static void main(String[] args) {
//        Searching test = new Searching();
//        Comparable[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1111, 1212, 1999, 2000,2322};
//        System.out.println(Searching.sequenceSearch(data,3,10));
//
//    }

    public void testSequenceSearch() throws Exception
    {
        Comparable[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1111, 1212, 1999, 2000,2322};
        assertEquals(true,Searching.sequenceSearch(data,2,14));
        assertEquals(false,Searching.sequenceSearch(data,11,14));
    }

    public void testFibonacciSearch() throws Exception
    {
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1111, 1212, 1999, 2000,2322};
        assertEquals(true,Searching.fibonacciSearch(data,2322));
        assertEquals(false,Searching.fibonacciSearch(data,11111));
        assertEquals(true,Searching.fibonacciSearch(data,1));
    }

    public void testBinarySearch() throws Exception
    {
        Comparable[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1111, 1212, 1999, 2000,2322};
        assertEquals(2322,Searching.binarySearch(data,2322));
        assertEquals(null,Searching.binarySearch(data,9999));
        assertEquals(1,Searching.binarySearch(data,1));
    }

    public void testInsertionSearch() throws Exception
    {
        int[] data = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 1111, 1212, 1999, 2000,2322};
        assertEquals(14,Searching.InsertionSearch(data,2322,0,13));
        assertEquals(13,Searching.InsertionSearch(data,2000,0,13));
        assertEquals(7,Searching.InsertionSearch(data,8,0,13));
        assertEquals(0,Searching.InsertionSearch(data,1,0,13));
    }
}