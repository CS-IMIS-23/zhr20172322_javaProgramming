package 大二上.第六周;

public class HashTest {
    public static void main(String[] args) {
        int[] hash = {11,78,10,1,3,2,4,21,43};

        HashSort.hashTable(hash,11);
        System.out.println("冲突次数为： " + HashSort.getCount());
        System.out.println("ASL为：13/9 = " + HashSort.getASL());  // 要求中没说需要输出，但还是输出了

        System.out.println("测试7是否在内： " + HashSort.HashSearch(hash,7));
        System.out.println("测试43是否在内： "+ HashSort.HashSearch(hash,43));

    }
}
