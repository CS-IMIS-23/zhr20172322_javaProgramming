package 大二上.第六周;

import 大二上.第二周.LinearNode;

public class HashSort {
    public static int count = 0;  //  冲突次数
//    public static double time = 0;
    public static double ASL = 13.0000000000/9.00000000;

    public static boolean HashSearch(int[] data, int element) {
        // 构建哈希表
        LinearNode[] hash = hashTable(data, 11);
        // 查找元素是否在哈希表中
        int k = element % 11;
        LinearNode current = hash[k];
        while (current != null && (int)current.getElement() != element) {
            current = current.getNext();
        }
        if (current == null) {
            return false;
        } else {

            return true;
        }
    }
    public static LinearNode[] hashTable(int[] data, int p){
        LinearNode[] hashtable = new LinearNode[p];
        int k; //k代表哈希函数计算的单元地址
        for (int i = 0; i < data.length; i++){
            LinearNode node = new LinearNode();
            node.setElement(data[i]);
            k = data[i] % p;
            if (hashtable[k] == null){
                hashtable[k] = node;  //  放入元素
            }else {
                LinearNode current = hashtable[k];
                while (current.getNext() != null){
                    current = current.getNext();  //  出现单元地址相同后，使用链地址法将冲突元素放入链的下一位
                }
                current.setNext(node);
                count++;  //  出现单元地址相同后，冲突次数加一
            }
        }
        return hashtable;
    }


    public static int getCount(){
        return count;
    }

    public static double getASL()
    {
        return ASL;
    }
}
