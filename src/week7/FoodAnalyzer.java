package week7;

//*************************************************************************************************
//  FoodAnalyzer.java                    Author: zhangyeye233
//
//  Demonstrates indirect access to inherited private members.
//*************************************************************************************************

public class FoodAnalyzer
{
    //---------------------------------------------------------------------------------------------
    //  Instantiates a Pizza objects and prints its calories per serving.
    //---------------------------------------------------------------------------------------------
    public static void main(String[] args) {
        Pizza special = new Pizza(275);

        System.out.println("Calories per serving: " + special.caloriesPerServing());
    }
}
