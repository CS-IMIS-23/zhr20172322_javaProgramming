package week6;

//  This is PP8.5
import java.util.Scanner;

public class PP85
{
        public static void main(String[] args)
        {
            Scanner scan = new Scanner(System.in);
            int[] num = new int[50];
            String another = "y";

            int a, sum1=0, c=0;

            while (another.equalsIgnoreCase("y")) // 实现输入“Y”循环
            {
                System.out.print("Enter a integer: ");
                a = scan.nextInt();
                sum1 += a;  //  求和
                num[c] = a;  //  数组分别赋值
                c++;

                System.out.print("Go on? (y/n)");
                another = scan.next();
            }

            double mean;
            mean = (double)sum1/c;
            System.out.println("The mean is: " + mean);

            double e, f, g, sd, sum2=0;
            for (int j=0;j < c;j++){
                f = num[j] - mean;  // 实现Xi - mean
                g = Math.pow(f,2); // 实现平方
                sum2 += g;  //  求和
            }
            sd = Math.sqrt(sum2);
            System.out.println("The sd is: " + sd);
        }
}