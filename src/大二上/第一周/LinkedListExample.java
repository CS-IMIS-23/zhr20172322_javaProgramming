package 大二上.第一周;

public class LinkedListExample
{
    public static void main(String[] args) {
        Student2 student = new Student2("张昊然",20172322,"爱学习");
        Student2 Head = student;
        PrintLinkedList(Head);  //  输出链表

        Student2 student1 = new Student2("张昊然",20172301,"爱学习");
        Student2 student2 = new Student2("张昊然",20172302,"爱学习");
        Student2 student3 = new Student2("张昊然",20172303,"爱学习");
        Student2 student4 = new Student2("张昊然",20172304,"爱学习");
        Student2 student5 = new Student2("张昊然",20172305,"爱学习");
        Student2 student6 = new Student2("张昊然",20172307,"爱学习");
        Student2 student7 = new Student2("张昊然",20172306,"爱学习");

        InsertNode(Head,student1);
        InsertNode(Head,student2);
        InsertNode(Head,student3);
        InsertNode(Head,student4);
        InsertNode(Head,student5);
        InsertNode(Head,student6);

        InsertNode(Head,student5,student6,student7);
        DeleteNode(Head,student2);

        PrintLinkedList(Head);  //  输出链表

//---------------------------------------------------------

    }

    public static void DeleteNode(Student2 Head, Student2 node)
    {
        Student2 PreNode = Head,CurrentNode = Head;
        while (CurrentNode != null)
        {
            if (CurrentNode.number != node.number)
            {
                PreNode = CurrentNode;
                CurrentNode = CurrentNode.next;

            }
            else {
                break;
            }
        }
        PreNode.next = CurrentNode.next;
    }

    public static void PrintLinkedList(Student2 Head) {
        Student2 node = Head;
        while (node != null)
        {
            System.out.println("姓名： " + node.name + "，学号： " + node.number + "，爱好： " + node.hobby );
            node = node.next;
        }
    }
    public static void InsertNode(Student2 Head, Student2 node)
    {
        //尾插法，在链表的尾部插入节点
        Student2 temp = Head;
            while (temp.next != null)
        {
            temp = temp.next;
        }
        temp.next = node;
        //头插法
//        node.next = Head;
//        Head = node;


//        node = Head;
//        while (node.next == null) {
//            node = node.next;
//        }
//        Student2 temp  = new Student2("1",123,"123");
//        temp.next = null;
//        node.next = temp;
    }
    public static void InsertNode(Student2 Head, Student2 node1, Student2 node2, Student2 node3)
    {
        Student2 point = Head;
        while((point.number != node1.number) && point != null)
        {
            point = point.next;
        }
        if (point.number == node1.number)
        {
            node3.next = point.next;
            point.next = node3;
        }
    }
}
