package 第一周;//***************************************************************************************
//  Einstein.java                 Author:zhangyeye233
//
//  Do=emonstrates a basic applet
//***************************************************************************************

import javax.swing.JApplet;
import java.awt.*;

public class Einstein extends JApplet
{
    //-----------------------------------------------------------------------------------
    //  Draws a quotation by Albert Einstein among some shapes.
    //----------------------------------------------------------------------------------
    public void paint(Graphics page)
    {
       page.drawRect(50, 50, 40, 40);    // Square
       page.drawRect(60, 80, 225, 30);   // Rectangle
       page.drawOval(75, 65, 20, 20);    // Circle
       page.drawLine(35, 60, 100, 120);  // Line

       page.drawString("Out of clutter, find simplicity.", 110, 70);
       page.drawString("-- Albert Einstein", 130, 100);
    }
}
