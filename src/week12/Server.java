package week12;

import 四则运算.Calculator;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String args[]) {
        Calculator In = new Calculator();
        ServerSocket SS = null;
        Socket SOS = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        try {
            SS = new ServerSocket(1013);
        } catch (IOException e1) {
            System.out.println(e1);
        }
        try {
            System.out.println("等待客户呼叫");
            SOS = SS.accept();   //  启动服务器，客户连接后进行下一步
            System.out.println("客户已连接");
            out = new DataOutputStream(SOS.getOutputStream());
            in = new DataInputStream(SOS.getInputStream());
            String leng = in.readUTF(); //  读取信息
//            System.out.println(leng);
            byte ctext[] = new byte[Integer.parseInt(leng)];

            int i = 0;
            while (i<Integer.parseInt(leng)) {
                String temp = in.readUTF();
                ctext[i] = Byte.parseByte(temp);
                i ++;
            }
            // 获取密钥
            FileInputStream f2 = new FileInputStream("keykb1.dat");
            int num2 = f2.available();
            byte[] keykb = new byte[num2];
            f2.read(keykb);
            SecretKeySpec k = new SecretKeySpec(keykb, "DESede");
            // 解密
            Cipher cp = Cipher.getInstance("DESede");
            cp.init(Cipher.DECRYPT_MODE, k);
            byte[] ptext = cp.doFinal(ctext);

            System.out.println("");
            // 显示明文
            String p = new String(ptext,"UTF-8");
            System.out.println("解密后需要计算的后缀表达式：" + p);
            System.out.println("结果为：" + In.evaluate(p));
            out.writeUTF(In.evaluate(p)+"");

        } catch (Exception e) {
            System.out.println("客户已断开" + e);
        }
    }
}
