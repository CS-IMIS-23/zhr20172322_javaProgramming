//  This is homework PP2.12

import java.util.Scanner;

public class PP212
{
    public static void main(String[] args)
    {
       double side, perimeter, area;

       Scanner scan = new Scanner(System.in);

       System.out.print("Enter the square side length: ");
       side = scan.nextDouble();

       perimeter = side*4;
       System.out.println("The square perimeter is: " + perimeter );          

       area = side*side;
       System.out.println("The square area is: " + area + "\n\t Author: zhangyeye233");
    }
}
