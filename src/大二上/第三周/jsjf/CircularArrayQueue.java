package 大二上.第三周.jsjf;

import 大二上.第三周.jsjf.exceptions.*;


public class CircularArrayQueue<T> implements QueueADT<T> {
    private final static int DEFAULT_CAPACITY = 100;
    private int front, rear, count;
    private T[] queue;


    public CircularArrayQueue(int initialCapacity) {
        front = rear = count = 0;
        queue = ((T[]) (new Object[initialCapacity]));
    }


    public CircularArrayQueue() {
        this(DEFAULT_CAPACITY);
    }


    @Override
    public void enqueue(T element) {
        if (size() == queue.length) {
            expandCapacity();
        }

        queue[rear] = element;
        rear = (rear + 1) % queue.length;

        count++;
    }

    /**
     * Creates a new array to store the contents of this queue with
     * twice the capacity of the old one.
     */
    private void expandCapacity() {
        T[] larger = (T[]) (new Object[queue.length * 2]);

        for (int scan = 0; scan < count; scan++) {
            larger[scan] = queue[front];
            front = (front + 1) % queue.length;
        }

        front = 0;
        rear = count;
        queue = larger;
    }


    @Override
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }

        T result = queue[front];
        queue[rear] = null;
        front = (front + 1) % queue.length;

        count--;

        return result;
    }

    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("queue");
        }
        T result = queue[front];

        return result;
    }


    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
//        return queue.length;

        return rear - front;
//        int b = 0;
//        for (int a = 0; queue[a] != null; a++) {
//            b++;
//        }
//        return b;
}



    @Override
    public void addInList(int index, T element) throws Exception {

    }

    @Override
    public String toString() {
        String r = "";

        for (int g = 0; g < size(); g++) {
            r += String.valueOf(queue[g] + " ");
        }
        return r;
    }
}