package 大二上.第九周.jsjf;

import java.util.Scanner;

public class FlightNetTest {
    public static void main(String[] args) {
        String start,end;
        FlightNet<String> a = new FlightNet<>();

        a.addCity("北京");
        a.addCity("上海");
        a.addCity("天津");
        a.addCity("重庆");
        a.addCity("London");
        a.addCity("Washington");
        a.addEdge(0,1,1300);
        a.addEdge(0,3,1600);
        a.addEdge(0,4,5555);
        a.addEdge(0,5,7777);
        a.addEdge(1,2,1200);
        a.addEdge(1,3,1120);
        a.addEdge(1,4,6767);
        a.addEdge(1,5,5656);
        a.addEdge(2,3,1100);
        a.addEdge(3,4,3000);
        a.addEdge(3,5,2999);
        a.addEdge(4,5,4300);


        System.out.println(a.toString());

        Scanner scan = new Scanner(System.in);
        System.out.println("请输入起始城市（地名）：");
        start=scan.nextLine();

        System.out.println("请输入终止城市（地名）：");
        end=scan.nextLine();

        a.getShortestPath(start,end);
        a.getShortestWeightPath(start,end);

    }
}
