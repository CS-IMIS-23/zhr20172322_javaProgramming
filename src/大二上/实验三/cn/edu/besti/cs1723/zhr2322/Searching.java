package 大二上.实验三.cn.edu.besti.cs1723.zhr2322;

public class Searching {
    public static <T> boolean linearSearch(T[] data, int min, int max, T target)
    {
        int index = min;
        boolean found = false;

        while (!found && index <= max)
        {
            found = data[index].equals(target);
            index++;
        }

        return found;
    }
    // 顺序查找
    public static<T> boolean sequenceSearch(T[] data,T value,int n)
    {
        int i;
        for(i=0; i<n; i++) {
            if(data[i]==value) {
                return true;
            }
        }
        return false;
    }
    //  二分查找
    //  如果查找到则返回找到的值，若无该元素则返回null
    public static Comparable binarySearch(Comparable[] data, Comparable target) {
        Comparable result = null;
        int first = 0, last = data.length - 1, mid;

        while (result == null && first <= last) {
            mid = (first + last) / 2;  // determine midpoint
            if (data[mid].compareTo(target) == 0) {
                result = data[mid];
            } else if (data[mid].compareTo(target) > 0) {
                last = mid - 1;
            } else {
                first = mid + 1;
            }
        }
        return result;
    }

    //插值查找
    public static int InsertionSearch(int[] a, int value, int low, int high) {
        int mid = low + (value - a[low]) / (a[high] - a[low]) * (high - low);
        if (a[mid] == value) {
            return mid;
        }
        if (a[mid] > value) {
            return InsertionSearch(a, value, low, mid - 1);
        } else {
            return InsertionSearch(a, value, mid + 1, high);
        }
    }

    public static boolean fibonacciSearch(int[] table, int keyWord) {
        //确定需要的斐波那契数
        int i = 0;
        while (getFibonacci(i) - 1 == table.length) {
            i++;
        }
        //开始查找
        int low = 0;
        int height = table.length - 1;
        while (low <= height) {
            int mid = low + getFibonacci(i - 1);
            if (table[mid] == keyWord) {
                return true;
            } else if (table[mid] > keyWord) {
                height = mid - 1;
                i--;
            } else if (table[mid] < keyWord) {
                low = mid + 1;
                i -= 2;
            }
        }
        return false;
    }

    //得到第n个斐波那契数
    public static int getFibonacci(int n) {
        int res = 0;
        if (n == 0) {
            res = 0;
        } else if (n == 1) {
            res = 1;
        } else {
            int first = 0;
            int second = 1;
            for (int i = 2; i <= n; i++) {
                res = first + second;
                first = second;
                second = res;
            }
        }
        return res;
    }
}
