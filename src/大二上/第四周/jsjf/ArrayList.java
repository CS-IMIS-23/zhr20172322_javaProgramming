package 大二上.第四周.jsjf;

import 大二上.第四周.jsjf.exceptions.*;

import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;


public abstract class ArrayList<T> implements ListADT<T>, Iterable<T>
{
    private final static int DEFAULT_CAPACITY = 100;
    private final static int NOT_FOUND = -1;
	
    protected int rear;
    protected T[] list; 
	protected int modCount;

    public ArrayList()
    {
        this(DEFAULT_CAPACITY);
    }


    public ArrayList(int initialCapacity)
    {
        rear = 0;
        list = (T[])(new Object[initialCapacity]);
		modCount = 0;
    }


    protected void expandCapacity()
    {
        T[] larger = (T[]) (new Object[size() * 2]);
        for (int scan = 0; scan < modCount; scan++) {
            larger[scan] = list[scan];
        }

        rear = size();
        list = larger;
    }

    @Override
    public T removeLast() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("ArrayList");
        }
        T result;
        result = list[rear];
        list[rear] = null;
        rear--;

        return result;
    }

    @Override
    public T removeFirst() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("ArrayList");
        }
        T result;
        result = list[0];
        rear--;

        for (int scan = 0; scan < size();scan++)
        {
            list[scan] = list[scan + 1];
        }
        list[rear] = null;
        return result;
    }

    @Override
    public T remove(T element)
    {
        T result;
        int index = find(element);

        if (index == NOT_FOUND) {
            throw new ElementNotFoundException("ArrayList");
        }

        result = list[index];
        rear--;
		
        // shift the appropriate elements 
        for (int scan=index; scan < rear; scan++) {
            list[scan] = list[scan+1];
        }
 
        list[rear] = null;
		modCount++;

        return result;
    }
   

    @Override
    public T first() throws EmptyCollectionException
    {
        return list[0];
    }


    @Override
    public T last() throws EmptyCollectionException
    {
        return list[rear-1];
    }


    @Override
    public boolean contains(T target)
    {
        return (find(target) != NOT_FOUND);
    }


    private int find(T target)
    {
        int scan = 0; 
		int result = NOT_FOUND;
 
        if (!isEmpty()) {
            while (result == NOT_FOUND && scan < rear) {
                if (target.equals(list[scan])) {
                    result = scan;
                } else {
                    scan++;
                }
            }
        }

        return result;
    }


    @Override
    public boolean isEmpty()
    {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }
 

    @Override
    public int size()
    {
        return rear;
    }

    @Override
    public String toString()
    {
        String r = "";

        for (int g = 0; g < size(); g++) {
            r += String.valueOf(list[g] + " ");
        }
        return r;
    }
	

    @Override
    public Iterator<T> iterator()
    {
        return new ArrayListIterator();
    }

	private class ArrayListIterator implements Iterator<T>
	{
		int iteratorModCount;
		int current;

		public ArrayListIterator()
		{
			iteratorModCount = modCount;
			current = 0;
		}
		

		@Override
        public boolean hasNext() throws ConcurrentModificationException
		{
			if (iteratorModCount != modCount) {
                throw new ConcurrentModificationException();
            }
			
			return (current < rear);
		}
		

		@Override
        public T next() throws ConcurrentModificationException
		{
			if (!hasNext()) {
                throw new NoSuchElementException();
            }
			
			current++;
			
			return list[current - 1];
		}

		@Override
        public void remove() throws UnsupportedOperationException
		{
			throw new UnsupportedOperationException();
		}
		
	}	
}
