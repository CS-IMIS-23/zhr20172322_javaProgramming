package week4;

// This is PP4.9.
public class PairOfDice
{
    private final int MAX = 6; // maximum face value.

    private int a, b;// current value showing on the pair of dice.

    private Die c,d; // c & d means dice facevalue.

    //---------------------------------------------------------------------------------------------
    //  Constructor: Set the initial face value.
    //---------------------------------------------------------------------------------------------
    public PairOfDice()
    {
        c = new Die();
        d = new Die();
    }

    //---------------------------------------------------------------------------------------------
    //  Rolls the die and returns the result.
    //---------------------------------------------------------------------------------------------
    public int RandomRoll()
    {
        a = c.roll();
        b = d.roll();
        return a + b;
    }
    public void setA(int a1)
    {
        a = a1;
    }
    public int getA()
    {
        return a;
    }
    public void setB(int b1)
    {
        b = b1;
    }
    public int getB()
    {
        return b;
    }
}
