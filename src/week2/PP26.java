// This is PP2.6

import java.util.Scanner;

public class PP26
{
    public static void main(String[] args)
    {
       double mile, km;

       Scanner scan = new Scanner(System.in);

       System.out.println("Please enter the miles: ");
       mile = scan.nextDouble();

       km = mile/1.60935 ;

       System.out.println("The km is: " + km);
    }
}
