package 大二上.实验三;

import junit.framework.TestCase;
import org.junit.Test;

public class SortingTest extends TestCase{

    @Test
    public void testselectionSort() {
        Comparable[] data = {1111,1212,1999,2000,2303,2322};//正序
        Sorting.selectionSort(data);
        Comparable[] a = data;
        assertEquals(data[1],a[1]);
        assertEquals(data[0],a[0]);
        assertEquals(data[2],a[2]);
        assertEquals(data[3],a[3]);
        assertEquals(data[4],a[4]);

        Comparable[] data1 = {2322,2303,2000,1999,1212,1111};//逆序
        Sorting.selectionSort(data1);
        assertEquals(data[1],data1[1]);
        assertEquals(data[0],data1[0]);
        assertEquals(data[2],data1[2]);
        assertEquals(data[3],data1[3]);
        assertEquals(data[4],data1[4]);
//        assertEquals(data,data1);
        Comparable[] data2 = {2322,2000,2303,1999,1212,1111};//乱序
        Sorting.selectionSort(data2);
        assertEquals(data[1],data2[1]);
        assertEquals(data[0],data2[0]);
        assertEquals(data[2],data2[2]);
        assertEquals(data[3],data2[3]);
        assertEquals(data[4],data2[4]);

        Comparable[] data3 = {2322,2303,1999,2000,1212,1111};//乱序
        Sorting.selectionSort(data3);
        assertEquals(data[1],data3[1]);
        assertEquals(data[0],data3[0]);
        assertEquals(data[2],data3[2]);
        assertEquals(data[3],data3[3]);
        assertEquals(data[4],data3[4]);
        Comparable[] data4 = {2322,2303,2000,1999,1111,1212};//乱序
        Sorting.selectionSort(data4);
        assertEquals(data[1],data4[1]);
        assertEquals(data[0],data4[0]);
        assertEquals(data[2],data4[2]);
        assertEquals(data[3],data4[3]);
        assertEquals(data[4],data4[4]);
        Comparable[] data5 = {2322,2303,1111,1999,1212,2000};//乱序
        Sorting.selectionSort(data5);
        assertEquals(data[1],data5[1]);
        assertEquals(data[0],data5[0]);
        assertEquals(data[2],data5[2]);
        assertEquals(data[3],data5[3]);
        assertEquals(data[4],data5[4]);
        Comparable[] data6 = {2322,2303,2000,1111,1212,1999};//乱序
        Sorting.selectionSort(data6);
        assertEquals(data[1],data6[1]);
        assertEquals(data[0],data6[0]);
        assertEquals(data[2],data6[2]);
        assertEquals(data[3],data6[3]);
        assertEquals(data[4],data6[4]);
        Comparable[] data7 = {1212,2303,2000,1999,2322,1111};//乱序
        Sorting.selectionSort(data7);
        assertEquals(data[1],data7[1]);
        assertEquals(data[0],data7[0]);
        assertEquals(data[2],data7[2]);
        assertEquals(data[3],data7[3]);
        assertEquals(data[4],data7[4]);
        Comparable[] data8 = {1111,2303,2000,1999,1212,2322};//乱序
        Sorting.selectionSort(data8);
        assertEquals(data[1],data8[1]);
        assertEquals(data[0],data8[0]);
        assertEquals(data[2],data8[2]);
        assertEquals(data[3],data8[3]);
        assertEquals(data[4],data8[4]);
        Comparable[] data9 = {2322,1111,1212,1999,2000,2303};//乱序
        Sorting.selectionSort(data9);
        assertEquals(data[1],data9[1]);
        assertEquals(data[0],data9[0]);
        assertEquals(data[2],data9[2]);
        assertEquals(data[3],data9[3]);
        assertEquals(data[4],data9[4]);

    }
}