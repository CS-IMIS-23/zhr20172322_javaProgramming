package week10.五月十四日课上测试;

//*******************************************************************
//  MagazineRack.java       Author: Lewis/Loftus
//
//  Driver to exercise the MagazineList collection.
//*******************************************************************

public class MagazineRack
{
   //----------------------------------------------------------------
   //  Creates a MagazineList object, adds several magazines to the
   //  list, then prints it .
   //----------------------------------------------------------------
   public static void main(String[] args)
   {    
      MagazineList rack = new MagazineList();
      
      rack.add(new Magazine("Time"));
      rack.add(new Magazine("Woodworking Today"));
      rack.add(new Magazine("Communications of the ACM"));
      rack.add(new Magazine("House and Garden"));
      rack.add(new Magazine("GQ"));
      rack.delete(new Magazine("Time"));
//      rack.insert(3,new Magazine("lalala"));
      
      System.out.println(rack); 
   }
}
