package 大二上.第一周;

import 大二上.第二周.LinkedStack;

public class test1
{
    public static void main(String[] args) {
        LinkedStack a = new LinkedStack();
        System.out.println(a.isEmpty());
        a.push("123");
        a.push("345");
        a.push("lalala");
        a.push("9763");
        System.out.println(a.isEmpty());
        System.out.println(a.size());
        System.out.println(a.peek());
        System.out.println(a.pop());
        System.out.println(a.peek());
        System.out.println(a.toString());
    }
}
