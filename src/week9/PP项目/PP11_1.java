package week9.PP项目;

import java.util.Scanner;

public class PP11_1
{
    public static void main(String[] args) throws StringTooLongException
    {
        String i ="";

        final int MAX = 20;

        Scanner scan = new Scanner(System.in);

        while (true){
        StringTooLongException pr = new StringTooLongException("输入的字符串过长！");

        System.out.println("输入一串字符串，我们将会保存你之前写入的字符串（最好别超过20个字符，不然程序就结束啦。输入DONE退出程序）：");
        String value = scan.next();
        if (value.equals("DONE"))
            break;
        i += value;

        if (i.length()>MAX)
            throw pr;
        else
            System.out.println("你输入的字符串是：" + i);
    }
    }
}
