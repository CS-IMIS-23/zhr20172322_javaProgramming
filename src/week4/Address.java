package week4;

//*************************************************************************************************
//  Address.java       Author: zhangyeye233
//
//  Represents a streets address.
//*************************************************************************************************

public class Address
{
    private String streetAddress, city, state;
    private long zipCode;

    //---------------------------------------------------------------------------------------------
    //  Constructor: Sets up this address with the specified data.
    //---------------------------------------------------------------------------------------------
    public Address(String street, String town, String st, long zip)
    {
        streetAddress = street;
        city = town;
        state = st;
        zipCode = zip;
    }

    //---------------------------------------------------------------------------------------------
    //  Returns a description of this Address object.
    //---------------------------------------------------------------------------------------------

    @Override
    public String toString()
    {
        String result;

        result = streetAddress + "\n";
        result += city + ", " + state + " " + zipCode;
        return result;
    }
}
