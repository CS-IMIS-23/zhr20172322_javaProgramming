package week5;

// This is PP6.7 part3.

public class PP67Stars3
{
    public static void main(String[] args)
    {
        final int MAX_ROWS = 10;

        for (int row = 1; row <= MAX_ROWS; row++)
        {
            for (int kongge = 0; kongge <= row; kongge++)
                System.out.print(" ");
            for (int stars = 10; stars >= row;stars--)
                System.out.print("*");

            System.out.println();
        }
    }
}
