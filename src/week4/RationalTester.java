package week4;

//*************************************************************************************************
//  RationalTester.java       Author: zhangyeye233
//
//  Drivee to exercise the use of multiple Rational objects.
//*************************************************************************************************

public class RationalTester
{
    //---------------------------------------------------------------------------------------------
    //  Creates some rational number objects and performs various operations on them.
    //---------------------------------------------------------------------------------------------
    public static void main(String[] args)
    {
        RationalNumber r1 = new RationalNumber(1, 2);
        RationalNumber r2 = new RationalNumber(2, 12);
        RationalNumber r3, r4, r5, r6, r7;

        System.out.println("First rational number: " + r1);
        System.out.println("Second rational number: " + r2);
        if (r1.compareTo(r2) < 0.0001)
            System.out.println("r1 < r2");
        else if (r1.compareTo(r2) > 0.0001)
            System.out.println("r1 > r2");
        else
            System.out.println("r1 = r2");


        r3 = r1.reciprocal();
        System.out.println("The reciprocal of r1 is: " + r3);

        r4 = r1.add(r2);
        r5 = r1.subtract(r2);
        r6 = r1.multiply(r2);
        r7 = r1.divide(r2);

        System.out.println("r1 + r2: " + r4);
        System.out.println("r1 - r2: " + r5);
        System.out.println("r1 * r2: " + r6);
        System.out.println("r1 / r2: " + r7);
    }
}
