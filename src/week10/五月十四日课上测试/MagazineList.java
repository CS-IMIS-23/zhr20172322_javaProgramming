package week10.五月十四日课上测试;
//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

public class MagazineList
{
   private MagazineNode list;

   //----------------------------------------------------------------
   //  Sets up an initially empty list of magazines.
   //----------------------------------------------------------------
   public MagazineList()
   {
      list = null;
   }

   //----------------------------------------------------------------
   //  Creates a new MagazineNode object and adds it to the end of
   //  the linked list.
   //----------------------------------------------------------------
   public void add(Magazine mag)
   {
      MagazineNode node = new MagazineNode(mag);
      MagazineNode current;

      if (list == null)
         list = node;
      else
      {
         current = list;
         while (current.next != null)
            current = current.next;
         current.next = node;
      }
   }


   public void delete(Magazine delNode)  //  删除（在Magazine中重写了equals方法！）
   {
      MagazineNode current = list;
      MagazineNode pre = list;
      while (!current.magazine.equals(delNode) && current.next != null)
      {
         current = current.next;
         pre = current;
      }
      if (!current.magazine.equals(delNode) && current.next == null)    //   输入的东西在链表中找不到
      {
         System.out.println("找不到节点，删除失败");
      }
      else if (current.magazine.equals(delNode) && current.next == null)  //  删除最后位置的文件
      {
          pre.next = null;
      }
      else if (current.magazine.equals(delNode) && current == list)   //  删除首个位置的文件
      {
         list = list.next;
      }
      else if (current.magazine.equals(delNode))   //  删除中间位置的文件
      {
         pre.next = current.next;
      }
   }

    public void insert(int index,Magazine mag)  //   使得新的magazine插到索引值对应的对象之前
    {
        MagazineNode node = list;
        int j = 0;
        while(node != null && j <= index - 3)   //  查找到第index-1个元素
        {
            node = node.next;
            j++;
        }
        MagazineNode Mag = new MagazineNode(mag);   //被插入的结点
        Mag.next = node.next;    //  使得插入的文件的next等于原本此位置node的next
        node.next = Mag;    //  使得node的next指向插入的文件

        switch (index) { //  发现之前如果index为1的时候总是导致插不到第一个位置，就向赵乾宸和王文彬同学询问了相应解决办法，突然发现极其巧妙！
                         //   说到底就是交换了两个文件的顺序，先令了一个缓存区temp，通过缓存区的帮助交换了两个文件的顺序。
            case 1:
        {
            Magazine temp = list.magazine;
            list.magazine = list.next.magazine;
            list.next.magazine = temp;
            break;
        }
            case 0:
            {
                System.out.println("索引值请不要输入0，这会导致程序的插入错误！");
                System.out.println("注意！以下是错误的结果！");
                break;
            }
        }
    }

    //----------------------------------------------------------------
   //  Returns this list of magazines as a string.
   //----------------------------------------------------------------
   public String toString()
   {
      String result = "";

      MagazineNode current = list;

      while (current != null)
      {
         result += current.magazine + "\n";
         current = current.next;
      }

      return result;
   }

   //*****************************************************************
   //  An inner class that represents a node in the magazine list.
   //  The public variables are accessed by the MagazineList class.
   //*****************************************************************
   private class MagazineNode
   {
      public Magazine magazine;
      public MagazineNode next;

      //--------------------------------------------------------------
      //  Sets up the node
      //--------------------------------------------------------------
      public MagazineNode(Magazine mag)
      {
         magazine = mag;
         next = null;
      }
   }
}
