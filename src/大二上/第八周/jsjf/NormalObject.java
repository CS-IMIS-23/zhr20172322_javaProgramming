package 大二上.第八周.jsjf;

public class NormalObject<T> implements Comparable<NormalObject>
{
    private int arrivalOrder;
    private T element;
    private static int nextOrder = 0;

    public NormalObject(T element)
    {
        this.element = element;
        arrivalOrder = nextOrder;
        nextOrder++;
    }

    public T getElement()
    {
        return element;
    }

    public int getArrivalOrder()
    {
        return arrivalOrder;
    }

    @Override
    public String toString()
    {
        return (element + "  "  + arrivalOrder);
    }

    @Override
    public int compareTo(NormalObject o) {
        int result;

        if (arrivalOrder > o.getArrivalOrder()) {
            result = 1;
        } else {
            result = -1;
        }

        return result;
    }
}
