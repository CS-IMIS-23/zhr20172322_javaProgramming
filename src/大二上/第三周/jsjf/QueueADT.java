package 大二上.第三周.jsjf;

/**
 * QueueADT defines the interface to a queue collection.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public interface QueueADT<T>
{
    public void enqueue(T element);
    public T dequeue();
    public T first();
    public boolean isEmpty();
    public int size();
    @Override
    public String toString();
}
