package 大二上.第八周.jsjf;

public class LinkedHeapTest {
    public static void main(String[] args) {
        LinkedHeap LH = new LinkedHeap();

        LH.addElement(55);
        LH.addElement(66);
        LH.addElement(77);
        LH.addElement(88);
        LH.addElement(99);
        LH.addElement(11);
        LH.addElement(22);
        LH.addElement(33);
        LH.addElement(44);

        System.out.println("查找最小值：" + LH.findMin());
        LH.removeMin();
        System.out.println("删除最小值之后的最小值：" + LH.findMin());

    }
}
