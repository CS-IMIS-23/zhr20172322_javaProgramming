# 20172322 2017-2018-2 《程序设计与数据结构》第七周学习总结

## 教材学习内容总结

- 学会了使用UML图来表示各类间关系，UML也可以使得编程的思路变得清晰。例如```#```代表可继承，```+```代表```public```，```-```代表```private```。
- 学会了使用```super```引用父类的构造方法，因为父类的构造方法不会继承给子类，但如果子类需要继承父类的构造方法需要使用```super```。
- 了解到当子类和父类有相同的方法名和签名时，子类方发将重写父类方法，例如书上例9.7、9.8，它的存在允许两个有继承关系的对象按照同名规则使用同名方法，当然```final```修饰符可以使得父类方法无法被重写。
- 了解到影子变量是父类与子类相同名称的变量，这种方法虽然合法但不提倡。
- 同一个父类的两个子类被称为同胞（***siblings***）
- Object类是几乎**所有类**的父类！
- 类的继承和接口的继承不可以重叠，接口不能用于派生新类，类不能用于派生接口。
- 父类的私有方法在子类中可以被间接引用，例如例9.10、9.11、9.12。

## 教材学习中的问题和解决过程
- 本周教材学习问题不多。
- 问题1：在书上有两句话“父类的私有方法或变量不能在子类中访问，或通过子类对象访问”和“所有的方法和变量（即使声明为private可见性）都将由子类继承”。开始没有看懂。
- 问题1解决方案：开始时是对访问和继承的概念不清晰，在仔细看书之后发现了区别，就像上一部分所提及的，被继承的方法可以被间接引用，第一句话所说的是直接引用。


## 代码调试中的问题和解决过程
- 问题1：在做作业PP9.1时遇到了两个问题，首先是在使用```b.setNum()```调用时导致面值被固定。![输入图片说明](https://gitee.com/uploads/images/2018/0422/224247_4d9b7168_1795082.png "PP9.1问题.png")![输入图片说明](https://gitee.com/uploads/images/2018/0422/224254_084ffe0b_1795082.png "PP9.1问题1.png")
- 问题1解决方案：之后发现好像```b.flip()```是现成的为啥不用。 :astonished: ![输入图片说明](https://gitee.com/uploads/images/2018/0422/224309_ba58265f_1795082.png "9.1问题解决.png")
- 问题2：还是PP9.1，解决上一个问题后满心欢喜，突然定睛一看怎么**没有计数！！！！！**，恐怖至极！![输入图片说明](https://gitee.com/uploads/images/2018/0422/224317_aa040a5d_1795082.png "PP9.1问题num.png")
- 问题2解决方案：仔细检查代码，发现有一处```return face```貌似有点倪端，赶紧将其改为```return num```**bigo**瞬间解决问题 :sunglasses: 
![输入图片说明](https://gitee.com/uploads/images/2018/0422/224323_e6d879a7_1795082.png "PP9.1解决.png")

## [代码托管](https://gitee.com/CS-IMIS-23/zhr20172322_javaProgramming.git)



## 上周考试错题总结
- 错题1及原因：平时看到的错误抛出总是A并没有遇到过B.C选项，便下意识选择了A。
![输入图片说明](https://gitee.com/uploads/images/2018/0422/224400_3a76090d_1795082.png "7.png")
- 理解情况：ArrayIndexOutOfBoundsException extends IndexOutOfBoundsException，用非法索引访问数组时抛出的异常。如果索引为负或大于等于数组大小，则该索引为非法索引。
也就是说角标异常！可能的原因是使用的角标大于等于数组的长度或为负数！

- 错题2及原因：对将一个数组中的所有元素复制到另一个数组中的方法不了解。![输入图片说明](https://gitee.com/uploads/images/2018/0422/224406_9ba69103_1795082.png "11.png")
- 理解情况：书上的例8.8给出了方法，即```a[x] = b[x]```类似，为啥不好好看书T T。看了就27了T T 


## 结对及互评

### 点评模板：
- 博客中值得学习的或问题：
    - 范雯琪同学的博客课本上的学习内容总结部分写得十分详细，一看就知道十分认真的学习了书本上的内容。
    - 错题部分虽然比较明了，但是使用非图片方法我觉得会浪费一些时间，直接以图片形式放上去或许可以有更多时间放在问题的理解上。
- 代码中值得学习的或问题：
    - commit提交的解释清晰明了，比我的要简洁一些，但是意思的表达上实际差不多，我觉得我应该学习，以节约时间。
    - 本周的课本上的代码和实验的代码并没有分开文件夹放置，导致今后找代码时存在些许困难，建议她分开放置。

### 点评过的同学博客和代码
- 本周结对学习情况
    - [20172303](http://www.cnblogs.com/PFrame/p/8850046.html)

    - 结对学习内容
        - 教会范雯琪同学使用UML图。
        - 在范雯琪同学在我编写PP9.1遇到困难时积极想我伸出了援手，使得我对本章我觉得一些难点豁然贯通！


## 其他

- 感悟：时间逐渐放宽，利用更多的时间去理解JAVA的~~乐趣所在~~

## 学习进度条

|            | 代码行数（新增/累积）| 博客量（新增/累积）|学习时间（新增/累积）|重要成长|
| --------   | :----------------:|:----------------:|:---------------:  |:-----:|
| 目标        | 5000行            |   30篇           | 400小时            |       |
| 第一周      | 242/242           |   1/4            | 13/13             |增加了对Java的兴趣       |
| 第二周      | 297/539           |   1/5            | 15/28             |发现目标并没有想象中那么难       |
| 第三周      | 315/854           |   2/7            | 18/46             |突然发觉JAVA的难度不小      |
| 第四周      | 1269/2123         |   1/8            | 50/96             |Java难到哭，但是有信心击败他！    |
| 第五周      | 737/2860          |   1/9            | 30/126            |难度的减小激发了学习动力    |
| 第六周      | 542/3402          |   2/11           | 20/146            |不能说难度变小！！！又难了T T   |
| 第七周      | 956/4357          |   1/12           | 35/181            |平平淡淡才是真    |


- 计划学习时间:20小时

- 实际学习时间:35小时

- 改进情况：commit的要求继续实施。

## 参考资料

-  [《Java程序设计与数据结构教程（第八版）》](https://book.douban.com/subject/26851579/)
-  [条件运算符](https://baike.baidu.com/item/%E6%9D%A1%E4%BB%B6%E8%BF%90%E7%AE%97%E7%AC%A6/6180633?fr=aladdin)