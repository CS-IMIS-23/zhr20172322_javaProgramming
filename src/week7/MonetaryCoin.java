package week7;

import week5.Coin;

import java.util.Random;

public class MonetaryCoin extends week7.Coin
{
    public int num;
    int[] value = {1,2,5,10};
    Random b = new Random();
    public int face()
    {
        int a;
        a = b.nextInt(4);
        num = value[a];
        return num;
    }

    @Override
    public String toString() {
        String c = "面值是" + num;
        return c;
    }
}
