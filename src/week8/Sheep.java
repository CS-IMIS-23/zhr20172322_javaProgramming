package week8;

public class Sheep extends Animal
{
    public Sheep(String name, int id)
    {
        super(name, id);
    }

    @Override
    public void eat()
    {
        System.out.println("好巧，我也吃草");
    }

    @Override
    public void sleep()
    {
        System.out.println("我站着睡");
    }

    @Override
    public void introduction()
    {
        System.out.println("我是888号");
    }
    public String toString()
    {
        String a;
        a = "我叫" + name + "，我的编号是" + id + ".";
        return a;
    }
}
