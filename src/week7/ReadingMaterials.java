package week7;

public class ReadingMaterials
{
    protected String names; // 名字
    protected int pages;   // 页数
    protected String AUTHOR;  //作者
    protected String Keys;  //关键词

    public void setNames(String NA)
    {
        names = NA;
    }

    public String getNames()
    {
        return names;
    }
    public void setPages(int PA)
    {
        pages = PA;
    }

    public int getPages()
    {
        return pages;
    }

    public void setAUTHOR(String Author)
    {
        AUTHOR = Author;
    }

    public String getAUTHOR()
    {
        return AUTHOR;
    }

    public void setKeys(String KEY)
    {
        KEY = Keys;
    }

    public String getKeys()
    {
        return Keys;
    }
}