package 大二上.实验二;


public class test {
    public static void main(String[] args) {

        LinkedBinaryTree a = new LinkedBinaryTree(9);
        LinkedBinaryTree b = new LinkedBinaryTree(10);
        LinkedBinaryTree test = new LinkedBinaryTree(2,a,b);


        System.out.println(test.isEmpty());
        System.out.println("--------------------------------------------");
        System.out.println(test.toString());
        System.out.println("--------------------------------------------");
        System.out.println(test.contains(9));
        System.out.println("--------------------------------------------");
        System.out.println(test.getLeft());
        System.out.println(test.getRight());
        System.out.println("--------------------------------------------");
        System.out.println(test.preOrder());
        System.out.println(test.postOrder());
        System.out.println("--------------------------------------------");

        test.removeRightSubtree();

        test.removeAllElements();
        System.out.println(test.isEmpty());
        System.out.println("--------------------------------------------");



    }
}
