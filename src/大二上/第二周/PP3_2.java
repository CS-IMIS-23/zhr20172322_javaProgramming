package 大二上.第二周;

import 大二上.第一周.ArrayStack;

import java.util.Scanner;
import java.util.StringTokenizer;

public class PP3_2 {
    public static void main(String[] args){

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a sentence: ");
        String sentence = scanner.nextLine();

        Transfer(sentence);
    }

    public static void Transfer(String str){
        ArrayStack stack = new ArrayStack();
        StringTokenizer tokenizer = new StringTokenizer(str); //   Tokenizer用于提取单词
        while (tokenizer.hasMoreTokens())
        {
            String transfer = tokenizer.nextToken();//  提取单词
            for (int i = 0;i < transfer.length();i++)
            {
                stack.push(transfer.charAt(i));  //  字母按顺序入栈
            }
            int j = 0;
            String s = "";
            while (j < transfer.length()){
                s += stack.pop() + "";  //   字符出栈以实现倒叙效果
                j++;
            }
            System.out.print(s + " ");  //  输出
        }
    }

}
