package week6;

// This is PP8.6.

public class AccountCollection
{
         Account[] acc;
        private int index;

        public AccountCollection()
        {
                acc = new Account[30];
                index = 0;
        }

        public void addAcount(String owner, int account, double initial)  // 建户
        {
                if (index > 30)
                        System.out.println("超出账户限制，无法添加！");  //  账户数少于30
                else {
                        acc[index] = new Account(owner, account, initial);
                        System.out.println("新账户的索引值为：" + index);
                        System.out.println("账户所有者：" + owner);
                        System.out.println("账户帐号：" + account);
                        System.out.println("账户余额：" + initial);
                        index++;
                }
        }

        public String saveMoney(double money, int index)   //  存钱
        {
                if (acc[index] != null)
                {
                        acc[index].deposit(money);
                        return "你给" + index + "账户存入" + money;
                } else
                        return "错误！账户不在范围内！";
        }

        public String getMoney(double money, double fee, int index)   //取钱
        {
                if (acc[index] != null)
                {
                        acc[index].withdraw(money, fee);
                        return "你从" + index + "账户取出" + money;
                } else
                        return "错误！账户不在范围内！";
        }

        public void addInterest()   //  加息
        {
                int index = 0;
                while (index < 30)
                {
                        if (acc[index] != null)
                                acc[index].addInterest();
                        index++;
                }
        }
}