package 大二上.实验一;

public class myarray {
    private long[] array;
    //表示有效数据的长度
    private int elements;

    public myarray() {
//        在构造方法里给数组赋值
        array = new long[50];
    }

    //接下来插入数据
    public void insert(int values) {
//第一次添加是为0的，即values
        array[elements] = values;
        elements++;//让她递增
//当第二次调用这儿添加的时候，array[elements]里面是为1,然后递增为2
    }

    //显示数据
    public void diplay() {
        System.out.print("[");
//用for循环遍历，i=0，下标为0
        for (int i = 0; i < elements; i++) {
            System.out.print(array[i] + "  ");
        }
        System.out.println("]");
    }

    public void selectionSort()
    {
//        myarray ma = null;
        int n = elements;
        for (int i = 0; i < n; i++)
        {
            int k = i;
            for (int j = i + 1;j<n;j++)
            {
                if (array[j]<array[k])
                {
                    k = j;
                }
            }

            if (k>1)
            {
                long temp = array[i];
                array[i] = array[k];
                array[k] = temp;
            }
            System.out.println("第" + (i+1)+"次排序后的结果是：");
            for (int h = 0 ; h<elements;h++)
            {
                System.out.print(array[h] + " ");
            }
            System.out.println("\n" + "此时元素个数是：" + elements);
        }
    }

    public long get(int index) {

        if (index >= elements || index < 0)//如果index大于有效元素，或者小于0
        {

            throw new ArrayIndexOutOfBoundsException();
        } else {
            return array[index];
        }
    }


    public void insertInarray (int index,int element) {

        if (index >= elements || index < 0)
        {

            throw new ArrayIndexOutOfBoundsException();
        }

        else {

            for (int i = elements; i > index-1; i--) {
//                array[index] = element;
                array[i+1] = array[i];
//                array[index+2] = array[index+1];

            }
            array[index] = element;
            elements++;
        }
    }

    public void delete(int index) {

        if (index >= elements || index < 0)
        {

            throw new ArrayIndexOutOfBoundsException();
        }

        else {

            for (int i = index; i < elements; i++) {

                array[i] = array[i + 1];
            }

            elements--;
        }
    }

    public int size()
    {
        return elements;
    }

    @Override
    public String toString() {
        String r = "";

        for (int g = 0; g < size(); g++) {
            r += String.valueOf(array[g] + " ");
        }
        return r;
    }
}
