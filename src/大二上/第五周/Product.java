package 大二上.第五周;

import java.io.Serializable;

/**
 * @author zhangyeye233
 */
public class Product implements Serializable,Comparable<Product>
{
    private String name;
    protected int price;
    private int weight;
    private int DateOfManufacture;// 生产日期
    private int count = 0;
    private LinearNode head, tail,current;

    public Product(String name, int price, int weight, int DateOfManufacture) {
        this.DateOfManufacture = DateOfManufacture;
        this.weight = weight;
        this.price = price;
        this.name = name;
    }

//    public void addFirst(Product product)
//    {
//        LinearNode<T> node = new LinearNode<T>((T) product);
//        current = head;
//        if (isEmpty())
//        {
//            head = node;
//        }
//        else {
//            tail.setNext(node);
//        }
//
//        count++;
//    }

//    public void addToTail(T element) {
//        LinearNode<T> node = new LinearNode<T>(element);
//
//        if (isEmpty()) {
//            head = node;
//        } else {
//            tail.setNext(node);
//        }
//
//        tail = node;
//        count++;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getDateOfManufacture() {
        return DateOfManufacture;
    }

    public void setDateOfManufacture(int dateOfManufacture) {
        DateOfManufacture = dateOfManufacture;
    }


    @Override
    public  String toString() {
        String result = "商品名称：" + name + "，商品价格：" + price + "元，商品重量：" + weight + "斤，生产日期：" + DateOfManufacture;
        System.out.println(result);
        return "" ;
    }

    @Override
    public int compareTo(Product Pr) {
        return Pr.getPrice() > price ? 1 : -1;
    }


}

