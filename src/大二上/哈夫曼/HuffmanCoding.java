package 大二上.哈夫曼;

import java.util.*;

public class HuffmanCoding<T> {
    public HuffmanNode<T> createTree(List<HuffmanNode<T>> nodes) {
        while (nodes.size() > 1) {
            Collections.sort(nodes);

            HuffmanNode<T> left = nodes.get(nodes.size() - 2);
            left.setCode(0 + "");  //  左子树为否（0）
            HuffmanNode<T> right = nodes.get(nodes.size() - 1);
            right.setCode(1 + "");  //  右子树为是（1）
            HuffmanNode<T> parent = new HuffmanNode<>(null, left.getWeight() + right.getWeight()); //   双亲结点的权值为左右孩子相加
            parent.setLeft(left);
            parent.setRight(right);
            nodes.remove(left);
            nodes.remove(right);
            nodes.add(parent);
        }
        return nodes.get(0);  //  返回根结点
    }

    //   广度优先遍历以赋值
    public List<HuffmanNode<T>> BFS(HuffmanNode<T> root) {
        List<HuffmanNode<T>> list = new ArrayList<>();
        Queue<HuffmanNode<T>> queue = new ArrayDeque<>();

        if (root != null) {
            //  将元素入队列
            queue.offer(root);
            root.getLeft().setCode(root.getCode() + "0");
            root.getRight().setCode(root.getCode() + "1");
        }

        //  转化为0和1
        while (!queue.isEmpty()) {
            list.add(queue.peek());
            HuffmanNode<T> node = queue.poll();  //  获取头结点
            if (node.getLeft() != null) {
                node.getLeft().setCode(node.getCode() + "0");  //  左为0
            }
            if (node.getRight() != null) {
                node.getRight().setCode(node.getCode() + "1");  //  右为1
            }

            if (node.getLeft() != null) {
                queue.offer(node.getLeft());
            }

            if (node.getRight() != null) {
                queue.offer(node.getRight());
            }
        }
        return list;
    }
}
