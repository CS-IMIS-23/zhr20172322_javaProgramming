package 数据库;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Test
{

    public static void main(String[] args) {
        //声明Connection对象
        Connection con;
        //驱动程序名
        String driver = "com.mysql.jdbc.Driver";
        //URL指向要访问的数据库名mydata
        String url = "jdbc:mysql://localhost:3306/world";
        //MySQL配置时的用户名
        String user = "root";
        //MySQL配置时的密码
        String password = "";
        //遍历查询结果集
        try {
            //加载驱动程序
            Class.forName(driver);
            //连接MySQL数据库
            con = DriverManager.getConnection(url,user,password);
            if(!con.isClosed())
                System.out.println("Succeeded connecting to the Database!");
            //用来执行SQL语句
            Statement statement = con.createStatement();
            //要执行的SQL语句
            String sql = "SELECT SUM(Population) FROM country WHERE country.Region=\"Middle East\" ";

            ResultSet rs = statement.executeQuery(sql);


            String name = null;
            String population = null;
            System.out.println();
            System.out.println("中东人口总和是：");
            while(rs.next())
            {
                population = rs.getString("SUM(Population)");
               //输出结果

                System.out.println(population);
            }
            rs.close();
        }
        catch(ClassNotFoundException e) {
            //数据库驱动类异常处理
            System.out.println("Sorry,can`t find the Driver!");
            e.printStackTrace();
        } catch(SQLException e) {
            //数据库连接失败异常处理
            e.printStackTrace();
        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }finally{
            System.out.println("数据库数据成功获取！！");
        }
    }
}
