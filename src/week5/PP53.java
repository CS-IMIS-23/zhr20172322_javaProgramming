package week5;

// This is PP5.3.

import java.util.Scanner;

public class PP53 {
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        int a;
        System.out.println("Please enter a integer: ");
        a = scan.nextInt();

        String b = a + "";

        int c = b.length();
        int odd = 0;
        int even = 0;
        int zero = 0;

        int d = 0;

        for (int i = 0; i < c; i++)
        {
            d = Integer.valueOf(b.substring(i, i + 1));
            if (d == 0)
            {
                zero++;
            }
            else
                if (d % 2 != 0 && d != 0)
                {
                odd++;
                }
            else
                if (d % 2 == 0 && d != 0)
                {
                even++;
                }
        }
        System.out.println("Even numbers have " + even);
        System.out.println("Odd number have " +  odd);
        System.out.println("The zero have " + zero);
    }
}

