// This is SR2.37

import java.util.Scanner;

public class SR237
{
    public static void main(String[] args)
    {
       int value = 0;

       Scanner scan = new Scanner(System.in);

       System.out.println("Please enter your age: ");
       value = scan.nextInt();

       System.out.println(value);
    }
}
