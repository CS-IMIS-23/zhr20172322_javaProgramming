package 大二上.实验二;

import 大二上.第六周.DecisionTree;

import java.io.FileNotFoundException;

/**
 * BackPainAnaylyzer demonstrates the use of a binary decision tree to 
 * diagnose back pain.
 */
public class BackPainAnalyzer
{
    /**
     *  Asks questions of the user to diagnose a medical problem.
     */
    public static void main (String[] args) throws FileNotFoundException
    {
        System.out.println ("按照你来看，这个人帅不帅？");

        DecisionTree expert = new DecisionTree("input.txt");
        expert.evaluate();

//        System.out.println("决策树的深度为： " + expert.getTree().getHeight());
//        System.out.println("决策树的叶结点数为： " + expert.getTree().CountLeaf(expert.getTree().getRootNode()));
    }
}