package week5;

// This is PP5.7.

import java.util.Scanner;
import java.util.Random;

public class PP57
{
    public static void main(String[] args)
    {
        int b, c, d = 0, e = 0, f = 0;

        Scanner scan = new Scanner(System.in);
        Random ran = new Random();
        String anthor = "y";

        while(anthor.equalsIgnoreCase("y"))
        {
        System.out.println("Welcome to playing this game: Rock-paper-scissors. \nIn this game, " +
                             "you choose a type of input and the computer will randomly select a type." +
                " computer.\n (1 means rock, 0 means scissors, -1 means paper.)  "
                + "\n When game over. We will tell you what type of computer choose.\n Please enter these number:");
        b = scan.nextInt();
        c = ran.nextInt(3) - 1;

            if (b - 1 <= 0.5 && b - 1 >= -0.5)
            {
                if (b - c - 1 <= 0.5 && b - c >= -0.5){
                    System.out.println("You win!");
                    d++;}
                if (b - c - 2 <= 0.5 && b - c - 2 >= -0.5){
                    System.out.println("Sorry, you lost!");
                    e++;}
                if (b - c <= 0.5 && b - c >= -0.5){
                    System.out.println("It's a draw!");
                    f++;}
            }
            else if
                    (b <= 0.5 && b >= -0.5)
            {
                if (b - c + 1 <= 0.5 && b - c + 1 >= -0.5){
                    System.out.println("Sorry, you lost!");
                    e++;}
                if (b - c <= 0.5 && b - c >= -0.5){
                    System.out.println("It's a draw!");
                    f++;}
                if (b - c - 1 <= 0.5 && b - c - 1 >= -0.5){
                    System.out.println("You win!");
                    d++;}
            }
            else if (b + 1 <= 0.5 && b + 1 >= -0.5)
            {
                if (b - c + 2 <= 0.5 && b - c + 2 >= -0.5){
                    System.out.println("You win!");
                    d++;}
                if (b - c + 1 <= 0.5 && b - c + 1 >= -0.5){
                    System.out.println("Sorry, you lost!");
                    e++;}
                if (b - c <= 0.5 && b - c >= -0.5){
                    System.out.println("It's a draw!");
                    f++;}
            }
            else
            System.out.println("Sorry! It's a wrong number.");
            System.out.println();
            System.out.println("The computer output is: " + c + ". 1 means rock, 0 means scissors, -1 means paper.");

            System.out.println();
            System.out.print("Go on?(y/n)");
            anthor = scan.next();
        }
        System.out.println("You win " + d + " round." + "\nYou lost " + e + " round." + "\nDraw " + f + " round.");
    }
}