package test.实验二;

import 实验二.Complex;
import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex x = new Complex(6,5);
    Complex y = new Complex(4,3);
    Complex z = new Complex(6,5);
    @Test
    public void testAdd(){                                          //  加法
        assertEquals(10.0,x.ComplexAdd(y).RealPart);
        assertEquals(8.0,x.ComplexAdd(y).ImagePart);
    }
    public void testSub(){                                          //  减法
        assertEquals(2.0,x.ComplexSub(y).RealPart);
        assertEquals(2.0,x.ComplexSub(y).ImagePart);
    }
    public void testMulti(){                                        //  乘法
        assertEquals(9.0,x.ComplexMulti(y).RealPart);
        assertEquals(38.0,x.ComplexMulti(y).ImagePart);
    }
    public void testDiv(){                                          //  除法
        assertEquals(1.56,x.ComplexDiv(y).RealPart);
        assertEquals(0.08,x.ComplexDiv(y).ImagePart);
    }
}