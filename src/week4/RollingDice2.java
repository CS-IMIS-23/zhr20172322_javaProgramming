package week4;

// This is PP4.9

public class RollingDice2
{
    public static void main(String[] args)
    {
        PairOfDice a;// a means dice.
        int sum;
        a = new PairOfDice();

        a.RandomRoll();
        sum = a.getA() +a.getB();
        System.out.println("The first dice: " + a.getA() + "\n" + "The second dice: " + a.getB() + "\n" +
                "The sum is: " + sum);
    }
}
