package 大二上.第一周;



public class EmptyCollectionException extends RuntimeException
{
    public EmptyCollectionException(String collection)
    {
        super("The " + collection + "is empty.");
    }
}
