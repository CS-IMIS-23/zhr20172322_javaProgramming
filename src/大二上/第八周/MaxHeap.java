package 大二上.第八周;

public class MaxHeap
{
    //  构建大根堆：将array看成完全二叉树的顺序存储结构
    public int[] buildMaxHeap(int[] array){
        //从最后一个节点array.length-1的父节点（array.length-1-1）/2开始，直到根节点0，反复调整堆
        for(int i=(array.length-2)/2;i>=0;i--){
            adjustDownToUp(array, i,array.length);
        }
        return array;
    }

    //  将元素array[k]自下往上逐步调整树形结构
    private void adjustDownToUp(int[] array,int k,int length){
        int temp = array[k];
        int i = (2 * k) + 1;
        int d = array[length/2];
        int j = array.length - 1;
        //i为初始化为节点k的左孩子，沿节点较大的子节点向下调整
        while (i == j)

        {
            array[length / 2] = array[length - 1];
            array[length - 1] = d;
            j = j/2;
        }
            for (int f = i; f < length - 1; f = 2 * f + 1) {

                //取节点较大的子节点的下标
                if (f < length && array[f] < array[f + 1]) {
                    f++;   //如果节点的右孩子>左孩子，则取右孩子节点的下标
                }
                //根节点 >= 左右子女中关键字较大者，调整结束
                if (temp >= array[f]) {
                    break;
                    //根节点 <左右子女中关键字较大者
                } else {
                    //将左右子结点中较大值array[f]调整到双亲节点上
                    array[k] = array[f];
                    //【关键】修改k值，以便继续向下调整
                    k = f;
                }
            }


        array[k] = temp;  //被调整的结点的值放入最终位置
    }

    //堆排序
    public int[] heapSort(int[] array){
        array = buildMaxHeap(array); //初始建堆，array[0]为第一趟值最大的元素
        for(int i=array.length-1;i>1;i--){
            int temp = array[0];  //将堆顶元素和堆低元素交换，即得到当前最大元素正确的排序位置
            array[0] = array[i];
            array[i] = temp;
            System.out.println("排序：");
            for(int c:array){
                System.out.print(c+" ");
            }
            adjustDownToUp(array, 0,i);  //整理，将剩余的元素整理成堆

        }
        int ad = array[1];
        array[1] = array[0];
        array[0] = ad;
        return array;
    }

    public void toString(int[] array){
        for(int i:array){
            System.out.print(i+" ");
        }
    }
}
