package 大二上.第八周.jsjf;

public class ArrayHeapTest {
    public static void main(String[] args) {
        ArrayHeap AH = new ArrayHeap();


        AH.addElement(55);
        AH.addElement(66);
        AH.addElement(77);
        AH.addElement(88);
        AH.addElement(99);
        AH.addElement(11);
        AH.addElement(22);
        AH.addElement(33);
        AH.addElement(44);

        System.out.println("堆是否为空？" + AH.isEmpty());
        System.out.println("查找最小值：" + AH.findMin());
        AH.removeMin();
        System.out.println("删除最小值后查找最小值：" + AH.findMin());
        System.out.println("此时元素总数为？" + AH.size());
        System.out.println("输出整个堆：" + AH.toString());
    }
}
