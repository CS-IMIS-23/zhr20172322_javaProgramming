package week4;

// This is PP4.5     Author: zhangyeye233

public class Car
{
    private String Name;
    private String Model;
    private int ProductionYear;
    private boolean YN;

    public Car(String na, String mo, int py)
    {
        Name = na;
        Model = mo;
        ProductionYear = py;
    }

    public void setName(String na)
    {
        Name = na;
    }

    public String getName()
    {
        return Name;
    }

    public void setModel(String mo)
    {
        Model = mo;
    }

    public String getModel()
    {
        return Model;
    }

    public void setProductionYear(int py)
    {
        ProductionYear = py;
    }

    public int getProductionYear()
    {
        return ProductionYear;
    }

    public String toString()
    {
        return "汽车厂商名称: " + Name + "型号: " + Model + "出产年份: " + ProductionYear;
    }

    public boolean isAntique()
    {
        if (ProductionYear < 1973)
        {
            YN = true;
        }
        else
            {
            YN = false;
        }
        return YN;
    }
}