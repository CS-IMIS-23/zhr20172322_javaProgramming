//  This is PP2.7

import java.util.Scanner;

public class PP27
{
    public static void main(String[] args)

    {
       int speed, distance;
       double time;

       Scanner scan = new Scanner(System.in);

       System.out.println("Enter the speed of your travel: (integer,km/h) ");
       speed = scan.nextInt();

       System.out.println("Enter the distance of your travel: (integer,km) ");
       distance = scan.nextInt();

       time = distance/speed;
       System.out.println("The time you speed to reach your destination when you travel: " + time +"hours.");
    }
}
