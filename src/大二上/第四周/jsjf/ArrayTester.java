package 大二上.第四周.jsjf;

public class ArrayTester {
    public static void main(String[] args) {
        System.out.println("有序列表测试：");
        ArrayOrderedList orderedList = new ArrayOrderedList();
        //有序列表
        orderedList.add("a");
        orderedList.add("b");
        orderedList.add("c");
        orderedList.add("d");
        orderedList.add("f");
        orderedList.add("k");
        System.out.println(orderedList.toString());
        System.out.println(orderedList.first());
        System.out.println(orderedList.last());
        System.out.println(orderedList.isEmpty());
        System.out.println(orderedList.contains("a"));
        System.out.println(orderedList.contains("z"));
        orderedList.remove("c");
        System.out.println(orderedList.toString());

        System.out.println("无序列表测试：");
        ArrayUnorderedList unorderedList = new ArrayUnorderedList();
        //无序列表
        unorderedList.addToFront("a");
        unorderedList.addToFront("b");
        unorderedList.addToFront("c");
        unorderedList.addToFront("d");
        unorderedList.addToFront("e");
        unorderedList.addToRear("z");
        unorderedList.addAfter("e","b");
        System.out.println(unorderedList.toString());
        System.out.println(unorderedList.size());
        System.out.println(unorderedList.last());
        unorderedList.remove("a");
        System.out.println(unorderedList.toString());
        unorderedList.removeFirst();
        System.out.println(unorderedList.toString());
        unorderedList.removeLast();
        System.out.println(unorderedList.toString());
    }
}
