package 大二上.第三周.jsjf;

public class testPP5_7 {
    public static void main(String[] args) {

        PP5_7<String> list = new PP5_7<>();
        list.insert("aaaa", 0);
        list.addAtTail("bbbb");
        list.insert("cccc", 0);
        // 在索引为1处插入一个新元素
        list.insert("dddd", 1);
        // 输出顺序线性表的元素
        System.out.println(list);
        // 删除索引为2处的元素
        list.delete(2);
        System.out.println(list);
        // 获取cccc字符串在顺序线性表中的位置
        System.out.println("cccc在顺序线性表中的位置：" + list.locate("cccc"));
        System.out.println("链表中索引1处的元素：" + list.get(1));
        list.remove();
        System.out.println("调用remove方法后的链表：" + list);
        list.delete(0);
        System.out.println("调用delete(0)后的链表：" + list);

    }
}
