package 大二上.第九周.jsjf;

import 大二上.第一周.StackADT;
import 大二上.第三周.jsjf.LinkedQueue;
import 大二上.第三周.jsjf.QueueADT;
import 大二上.第二周.LinkedStack;
import 大二上.第四周.jsjf.ArrayUnorderedList;
import 大二上.第四周.jsjf.UnorderedListADT;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListGraph implements GraphADT {
        private ArrayList<VertexNode> nodelist;
        private int MaxCount;
        private int ModCount;
        private int a;

        public ListGraph()
        {
            MaxCount = 0;
            ModCount = 0;
            a = 0;
            nodelist = new ArrayList<VertexNode>();
        }



    @Override
    public void addVertex(Object vertex) {
            VertexNode a = new VertexNode(vertex);
            nodelist.add(a);
            MaxCount++;
            ModCount++;
    }

    @Override
    public void removeVertex(Object vertex) {
        int i = 0;
        while (nodelist.get(i).getElement() != vertex) {
            i++;
        }
        nodelist.remove(i);
        MaxCount--;
        ModCount--;
        for (int a = 0; a < MaxCount; a++) {
            VertexNode temp = nodelist.get(a);
            while (temp.getNext() != null) {
                if (temp.getNext().getElement() == vertex)//找到目标元素的前一位
                {
                    temp.setNext(temp.getNext().getNext());//前一位的下一位跳过目标元素，将其删除
                }
                temp = temp.getNext();
            }
            break;
        }
    }

    @Override
    public void addEdge(Object vertex1, Object vertex2) {        int i = 0;
        while (nodelist.get(i).getElement() != vertex1) {//找添加位置
            i++;
        }
        VertexNode temp = nodelist.get(i);//temp表示结点1
        while (temp.getNext() != null) {//找到temp链表的末端
            temp = temp.getNext();
        }
        temp.setNext(new VertexNode(vertex2));//在末端添加结点2，即表示建立联系

        int j = 0;//同理在结点2也应与结点1建立联系
        while (nodelist.get(j).getElement() != vertex2) {
            j++;
        }
        VertexNode temp1 = nodelist.get(j);
        while (temp1.getNext() != null) {
            temp1 = temp1.getNext();
        }
        temp1.setNext(new VertexNode(vertex1));
    }

    @Override
    public void removeEdge(Object vertex1, Object vertex2) {
        int i = 0;
        while (nodelist.get(i).getElement() != vertex1) {
            i++;
        }//找到结点1的索引
        VertexNode temp = nodelist.get(i);
        while (temp.getNext().getElement() != vertex2) {
            temp = temp.getNext();//在结点1的链表中找到结点2的前一结点
        }
        if (temp.getNext().getNext() == null) {//如果结点2是结点1链表的最后一个，直接将后面的设为空
            temp.setNext(null);
        } else {
            temp.getNext().setNext(temp.getNext().getNext());//如果要删除的在链表中间则跳过删除
        }
    }

    protected boolean indexIsValid(int index) {
        if (index < size()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Iterator iteratorBFS(Object startVertex) {
        return iteratorBFS(getIndex(startVertex));
    }

    private Iterator iteratorBFS(int startIndex) {
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT resultList = new ArrayUnorderedList();

        //索引无效，返回空
        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[MaxCount];
        //把所有顶点设为false，白色
        for (int i = 0; i < MaxCount; i++) {
            visited[i] = false;
        }

        //进入队列的为true，即访问过的，灰色
        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty()) {
            //出队列涂黑存入resultList中
            x = traversalQueue.dequeue();
            resultList.addToRear( nodelist.get(x).getElement());

            //如果进入resultList的顶点还有相邻的未访问过的顶点，将其涂灰入队
            for (int i = 0; i < MaxCount; i++) {
                if (hasEdge(x, i) && !visited[i]) {
                    traversalQueue.enqueue(i);
                    visited[i] = true;
                    a++;
                }
            }
        }
        return new GraphIterator(resultList.iterator());
    }


    @Override
    public Iterator iteratorDFS(Object startVertex) {
        return iteratorDFS(getIndex(startVertex));
    }

    public Iterator iteratorDFS(int startIndex) {
        Integer x;
        boolean found;
        Object[] vertices =  (new Object[MaxCount]);
        StackADT<Integer> traversalStack = new LinkedStack<Integer>();
        UnorderedListADT resultList = new ArrayUnorderedList();
        boolean[] visited = new boolean[MaxCount];

        if (!indexIsValid(startIndex)) {
            return resultList.iterator();
        }

        for (int i = 0; i < MaxCount; i++) {
            visited[i] = false;
        }

        traversalStack.push(startIndex);
        resultList.addToRear( nodelist.get(startIndex).getElement());
        visited[startIndex] = true;

        while (!traversalStack.isEmpty()) {
            x = traversalStack.peek();
            found = false;

            for (int i = 0; (i < MaxCount) && !found; i++) {
                if (hasEdge(x, i) && !visited[i]) {
                    traversalStack.push(i);
                    resultList.addToRear( nodelist.get(i).getElement());
                    visited[i] = true;
                    found = true;
                }
            }
            if (!found && !traversalStack.isEmpty()) {
                traversalStack.pop();
            }
        }
        for (int i = 0; i < MaxCount; i++) {
            if (visited[i] == false) {
                resultList.addToRear(nodelist.get(i).getElement());
            }
        }
        return new GraphIterator(resultList.iterator());
    }

    public boolean hasEdge(int a, int b) {
        if (a == b) {
            return false;
        }
        VertexNode vertex1 = nodelist.get(a);
        VertexNode vertex2 = nodelist.get(b);
        while (vertex1 != null) {
            if (vertex1.getElement() == vertex2.getElement()) {
                return true;
            }
            vertex1 = vertex1.getNext();
        }
        return false;
    }

    public int getIndex(Object vertex) {
        int i = 0;
        while (i < MaxCount) {
            if (nodelist.get(i).getElement() != vertex) {
                i++;
            } else {
                break;
            }
        }
        return i;
    }

    @Override
    public Iterator iteratorShortestPath(Object startVertex, Object targetVertex) {
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    private Iterator iteratorShortestPath(int startIndex, int targetIndex)
    {
        UnorderedListADT resultList = new ArrayUnorderedList();
        //如果索引值都无效，返回空
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        //it表示构成startindex和targetindex之间最短路径的顶点集，并存储在resultlist链表中
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);
        while (it.hasNext()) {
            resultList.addToRear(nodelist.get(((Integer)it.next())).getElement());
        }
        return new GraphIterator(resultList.iterator());
    }

    private Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex)
    {
        int index = startIndex;
        int[] pathLength = new int[MaxCount];//路径长度数组
        int[] predecessor = new int[MaxCount];//前驱结点
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList = new ArrayUnorderedList<Integer>();

        //如果索引无效或起始终点为同一索引，返回空
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex)) {
            return resultList.iterator();
        }

        boolean[] visited = new boolean[MaxCount];//访问过为true，染灰

        //先标记为都没访问过，染白
        for (int i = 0; i < MaxCount; i++) {
            visited[i] = false;
        }

        //将起始值入队标为访问过，染灰
        traversalQueue.enqueue(Integer.valueOf(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;//路径长度为0
        predecessor[startIndex] = -1;//前驱结点为-1位置

        //如果还有访问过的灰色，并且未找到目标索引
        while (!traversalQueue.isEmpty() && (index != targetIndex))
        {
            index = (traversalQueue.dequeue()).intValue();//出队列染黑，index储存其元素值

            //如果有其他结点与index有联系却没有被访问过的，入队染灰
            for (int i = 0; i < MaxCount; i++)
            {
                if (hasEdge(index,i) && !visited[i])
                {
                    pathLength[i] = pathLength[index] + 1;//长度加一
                    predecessor[i] = index;//index为其前驱结点，predecessor中存储最短路径顶点
                    traversalQueue.enqueue(Integer.valueOf(i));//进队染灰
                    visited[i] = true;
                }
            }
        }
        //如果index不是目标索引，返回空
        if (index != targetIndex) {
            return resultList.iterator();
        }

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(Integer.valueOf(index));//目标索引入栈
        do
        {//index不是起始索引值的元素值时，index表示其前驱结点的元素值，将结点的值依次入栈
            index = predecessor[index];
            stack.push(Integer.valueOf(index));
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToRear(((Integer)stack.pop()));//栈不为空时，弹出结点添加到resultlist链表
        }

        return new GraphIndexIterator(resultList.iterator());//resultlist中储存的就是最短路径的顶点集
    }

    @Override
    public boolean isEmpty() {
        if (size() != 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean isConnected() {
        boolean result = true;
        for(int i=0;i<MaxCount;i++){
            int temp=0;
            temp = getSizeOfIterator(iteratorBFS(i));
            if(temp!=a)
            {
                result = false;
                break;
            }
        }
        return result;
    }

    private int getSizeOfIterator(Iterator iterator) {
        int size = 0;
        while(iterator.hasNext()){
            size++;
            iterator.next();
        }
        return size;
    }

    @Override
    public int size() {
        return MaxCount;
    }

    // 输出链表图
    public void printNodeList() {
        // 遍历每个顶点
        for (int i = 0; i < nodelist.size(); i++) {
            // 输出当前顶点编号

            // 获取当前节点下的所有节点
            VertexNode nodes = nodelist.get(i);
            System.out.print("顶点：" + nodelist.get(i).getElement());

            // 遍历当前节点下所有的节点
            while (nodes.getNext() != null) {
                System.out.print(" --> " + nodes.getNext().getElement());
                nodes = nodes.getNext();
            }
            System.out.println(";");
        }
    }

    protected class GraphIterator implements Iterator {
        private int expectedModCount;
        private Iterator iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public GraphIterator(Iterator iter) {
            this.iter = iter;
            expectedModCount = ModCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return true if this iterator has at least one more element to deliver
         * in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *                                         while the iterator is in use
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(ModCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        @Override
        public Object next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Inner class to represent an iterator over the indexes of this graph
     */
    protected class GraphIndexIterator implements Iterator<Integer> {
        private int expectedModCount;
        private Iterator<Integer> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public GraphIndexIterator(Iterator<Integer> iter) {
            this.iter = iter;
            expectedModCount = ModCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return true if this iterator has at least one more element to deliver
         * in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *                                         while the iterator is in use
         */
        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(ModCount == expectedModCount)) {
                throw new ConcurrentModificationException();
            }

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        @Override
        public Integer next() throws NoSuchElementException {
            if (hasNext()) {
                return (iter.next());
            } else {
                throw new NoSuchElementException();
            }
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
