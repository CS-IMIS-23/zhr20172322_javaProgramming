package week4;

// This is PP4.7.

public class Book {
    private String bookname;
    private String author;
    private String press;
    private String copyrightdate;

    public Book(String book, String au, String pr, String copy)
    {
        bookname = book;
        author = au;
        press = pr;
        copyrightdate = copy;
    }
    public void setBookname(String book)
    {
        bookname = book;
    }
    public String getBookname()
    {
        return bookname ;
    }
    public void setAuthor(String au)
    {
        author = au;
    }
    public String getAuthor()
    {
        return author;
    }
    public void setPress(String pr)
    {
        press = pr;
    }
    public String getPress(String pr)
    {
        return press;
    }
    public void setCopyrightdate(String copy)
    {
        copyrightdate = copy;
    }
    public String getCopyrightdate(String copy)
    {
        return copyrightdate;
    }
    public String toString() {
        return "Bookname: " + bookname + "\n\t" + "Author: " + author +
                "\n\t" + "Press: " + press + "\n\t" + "Copyrightdate: " + copyrightdate;
    }
}
