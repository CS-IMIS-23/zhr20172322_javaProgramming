package 大二上.第二周;

import 大二上.第一周.ArrayStack;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;
import java.util.StringTokenizer;

public class PP3_8 {
    public static void main(String[] args) {
        String sentence,token;

        Scanner scanner = new Scanner(System.in);
        System.out.print("输入一些元素，用空格隔开: ");
        sentence = scanner.nextLine();

        //  将字符串转化成栈
        StringTokenizer tokenizer = new StringTokenizer(sentence);
        ArrayStack stack = new ArrayStack();
        for (int a = 0;a < sentence.length(); a++ ){
            while (tokenizer.hasMoreTokens()) {
                token = tokenizer.nextToken();
                stack.push(token);
            }
        }

        //出栈
        String reversal = "倒叙为： ";
        int c = stack.size();
        for (int b = 0;b < c;b++){
            reversal += stack.pop() + " ";
        }
        System.out.println(reversal);
    }
}
