package week6;

//  This is PP8.7.
import java.util.Scanner;

public class PP86
{
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a;
        int b;
        AccountCollection acc = new AccountCollection();
        System.out.println("请输入你想建立的账户名称： (账户余额在建立账户时为0)");
        a = scan.next();
        System.out.println("请输入你想建立的账户帐号： (账户余额在建立账户时为0)");
        b = scan.nextInt();
        acc.addAcount(a,b,0);  //  建立账户
        System.out.println(acc.acc[0]);

        int c,d;
        System.out.println("请输入你想存入的数额： ");
        c = scan.nextInt();
        System.out.println("请输入你所想存入的账户的索引值（索引值在建立账户时提及，烦请输入该值）： ");
        d = scan.nextInt();
        acc.saveMoney(c,d);//  存钱
        System.out.println(acc.acc[0]);

        int e,f;
        System.out.println("请输入你想取出的数额(手续费为0.01%）：");
        e = scan.nextInt();
        System.out.println("请输入你所想取出的账户的索引值（索引值在建立账户时提及，烦请输入该值）：");
        f = scan.nextInt();
        acc.getMoney(e,e*0.0001,0);//   取钱
        System.out.println(acc.acc[0]);

        acc.addInterest();
        System.out.println("在一次加息后（利息为3%）");
        System.out.println(acc.acc[0]);
    }
}
