package 大二上.第四周.jsjf;

public class ListTester {
    public static void main(String[] args) {
        LinkedOrderedList a = new LinkedOrderedList();
        System.out.println("有序列表：");
        a.add(23);
        a.add(12);
        a.add(765);
        a.add(567);
        a.add(89);
        a.add(34);
        System.out.println(a.toString());
        System.out.println(a.size());
        System.out.println(a.isEmpty());
        System.out.println(a.first());
        a.removeFirst();
        a.removeLast();
        System.out.println(a.toString());
        System.out.println(a.first());
        System.out.println(a.last());
        System.out.println(a.size());

        System.out.println(a.remove(23));
        System.out.println(a.toString());


        System.out.println("无序列表：");
        LinkedUnorderedList b = new LinkedUnorderedList();
        b.addToFront(123);
        b.addToFront(456);
        b.addToFront(234);
        b.addToFront(999);
        b.addToRear(333);
        b.addAfter(679,456);
        System.out.println(b.toString());
        System.out.println(b.size());
        System.out.println(b.isEmpty());
        System.out.println(b.first());
        b.removeFirst();
        b.removeLast();
        System.out.println(b.toString());
        System.out.println(b.first());
        System.out.println(b.last());
        System.out.println(b.size());
    }
}
