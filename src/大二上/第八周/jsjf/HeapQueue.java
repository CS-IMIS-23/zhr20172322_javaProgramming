package 大二上.第八周.jsjf;

public class HeapQueue<T> extends ArrayHeap<NormalObject<T>>
{
    public HeapQueue()
    {
        super();
    }

    public void addelement(T object)
    {
        NormalObject<T> obj = new NormalObject<T>(object);
        super.addElement(obj);
    }

    public T removeNext()
    {
        NormalObject<T> obj = (NormalObject<T>)super.removeMin();
        return obj.getElement();
    }
}

