package week7;

//*************************************************************************************************
//  Pizza.java               Author: zhangyeye233
//
//  Represents a pizza, which is a food item. Used wo demonstrate indirect referencing through
//  inheritance.
//*************************************************************************************************

public class Pizza extends FoodItem
{
    //---------------------------------------------------------------------------------------------
    //  Sets up a pizza with the specified amount of fat (assumes eight serving).
    //---------------------------------------------------------------------------------------------
    public Pizza(int fatGrams)
    {
        super(fatGrams, 8);
    }
}
