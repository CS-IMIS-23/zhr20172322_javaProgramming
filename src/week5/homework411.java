package week5;

// This homework with "while"

import java.util.Scanner;

public class homework411
{
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        int n,a,b;

        System.out.println("Please enter a integer(n): ");
        n = scan.nextInt();
        a = 1;
        b = n;
        while(a<n)
        {
            b *= a;
            a++;
        }
        System.out.println("n! = " + b);
    }
}
