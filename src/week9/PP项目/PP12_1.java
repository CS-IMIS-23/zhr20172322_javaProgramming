package week9.PP项目;

import java.util.Scanner;

public class PP12_1
{
    public static void main(String[] args) {
        String another;

        Scanner scan = new Scanner(System.in);

            while (true)
            {
                System.out.print("Enter a potential palindrome:") ;
                String palindrome = scan.next() ;
                if (isPalindrome(palindrome))
                    System.out.println("That string IS a palindrome.");
                else
                    System.out.println("That string is NOT a palindrome.");
                System.out.println("Test another palindrome(y/n)?"); //   其实你只要不输入n都会继续运行的，嘿嘿嘿嘿
                another = scan.next();
                if (another.equalsIgnoreCase("n"))
                    break;
            }
    }

        public static boolean isPalindrome(String str) //   使用递归的方式实现判断是否为回文
        {
            if(str.length()==1)  //只有一个字符时必定为回文。
                return true ;
            else if(str.length()==2)
            {
                if(str.charAt(0)==str.charAt(str.length()-1))// 判断第一个和最后一个字符是否相等
                    return true ;
                else
                    return false ;
            }
            else if(str.charAt(0)!=str.charAt(str.length()-1)) //不相等则返回错误。
                return false ;
            else
                return isPalindrome(str.substring(1,str.length()-1)) ; //递归实现比较
        }
}
