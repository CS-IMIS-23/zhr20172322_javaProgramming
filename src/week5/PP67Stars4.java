package week5;

// This is PP6.7 part4.

public class PP67Stars4
{
    public static void main(String[] args) {
        final int MAX_ROWS = 5;

        for (int row = 1; row <= MAX_ROWS; row++)
        {
            for (int kongge = 4; kongge >= row; kongge--)
                System.out.print(" ");
            for (int stars = 1; stars <= row;stars++)
                System.out.print("*");
            for (int a =2; a <= row; a++)
                System.out.print("*");

            System.out.println();
        }
        for (int row = 1; row <= MAX_ROWS; row++)
        {
            for (int kongge = 2; kongge <= row; kongge++)
                System.out.print(" ");
            for (int stars = 5; stars >= row;stars--)
                System.out.print("*");
            for (int a =4; a >= row; a--)
                System.out.print("*");

            System.out.println();
        }
    }
}
