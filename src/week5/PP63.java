package week5;

// This is PP6.3

public class PP63
{
    public static void main(String[] args)
    {
    int x, y;

    for (x = 0; x <= 12; x++)
    {
        for (y = 1; y <= x; y++)
        {
            System.out.print(y + "*" + x + "=" + x * y + "\t");
        }
        System.out.println();
    }
}
}
