package 大二上.第九周.jsjf;

public class VertexNode<T> {
    private VertexNode next;
    private T element;

    public VertexNode() {
        next = null;
        element = null;
    }

    public VertexNode(T e)
    {
        next = null;
        element = e;
    }

    public VertexNode<T> getNext() {
        return next;
    }
    public void setNext(VertexNode<T> node)
    {
        next = node;
    }

    public T getElement()
    {
        return element;
    }

    public void setElement(T elem)
    {
        element = elem;
    }

    public String tostring(VertexNode node){
        String res = "";
        while (node!=null) {
            res += node.getElement() + " ";
            node = node.getNext();
        }
        return res;
    }

}