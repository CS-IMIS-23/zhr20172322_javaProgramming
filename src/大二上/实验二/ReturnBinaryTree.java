package 大二上.实验二;

import 大二上.第六周.jsjf.BinaryTreeNode;

public class ReturnBinaryTree extends LinkedBinaryTree {
    private BinaryTreeNode root;


    public ReturnBinaryTree() {
        root = null;
    }

    public void postOrder(BinaryTreeNode localRoot) {
        if (localRoot != null) {
            postOrder(localRoot.left);
            postOrder(localRoot.right);
            System.out.print(localRoot.element + " ");
        }
    }


    public void postOrder1() {
        this.postOrder(this.root);
    }

    public void initTree(String[] preOrder, String[] inOrder) {
        this.root = this.initTree(preOrder, 0, preOrder.length - 1, inOrder, 0, inOrder.length - 1);
    }

    public BinaryTreeNode initTree(String[] preOrder, int start1, int end1, String[] inOrder, int start2, int end2) {
        if (start1 > end1 || start2 > end2) {
            return null;
        }
        String rootData = preOrder[start1];
        BinaryTreeNode head = new BinaryTreeNode(rootData);
        //找到根节点所在位置
        int rootIndex = findIndexInArray(inOrder, rootData, start2, end2);
        //构建左子树
        BinaryTreeNode left = initTree(preOrder, start1 + 1, start1 + rootIndex - start2, inOrder, start2, rootIndex - 1);
        //构建右子树
        BinaryTreeNode right = initTree(preOrder, start1 + rootIndex - start2 + 1, end1, inOrder, rootIndex + 1, end2);
        head.left = left;
        head.right = right;
        return head;
    }

    public int findIndexInArray(String[] a, String x, int begin, int end) {
        for (int i = begin; i <= end; i++) {
            if (x == a[i]) {
                return i;
            }
        }
        return -1;
    }
}


