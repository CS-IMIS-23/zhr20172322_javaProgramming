// This is a comment.
/* This is anther comment. */

// This is acomment on a single line.

//-------------------------------------------------------------------------------------------------
// Some comments such as those above methods or classes
// deserve to be blocked off to focus special attentiomn
// on a particular aspect of your code. Note that each of
// these line is technically a separate comment.
//-------------------------------------------------------------------------------------------------

/*
    This is one comment
    that spans several lines.
*/

public class booktest1
{
    public static void main(String[] args)
    {
       System.out.println("Monthly Report"); // always use this title
       System.out.println("Hello"); // prints hello
       System.out.println("test"); // change this later
    }
}
