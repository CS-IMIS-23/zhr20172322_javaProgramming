package 大二上.第八周;

public class QueueTest {
    public static void main(String[] args) {
        PriorityQueue test = new PriorityQueue();

        test.addElement("爱",2);
        test.addElement("你",3);
        test.addElement("我",1);

        String result = "";
        for (int i = 0;i<test.length();i++)
        {
            result += test.removeNext() + " ";
        }
        System.out.println(result);
    }
}
