package test.实验二;

import junit.framework.TestCase;
import org.junit.Test;
import 实验二.StringBufferDemo;
import static org.junit.Assert.*;


public class StringBufferDemoTest extends TestCase
{
    StringBuffer a = new StringBuffer("BayernMunich"); //测试12个字符(<=16)
    StringBuffer b = new StringBuffer("BayernMunichBayernMunich"); // 测试24个字符(>16&&<=34)
    StringBuffer c = new StringBuffer("BayernMunichBayernMunichBayernMunich"); //  测试36个字符(>=34)
    @Test
    public void testcharAt() throws Exception
    {
        assertEquals('B',a.charAt(0));
        assertEquals('e',a.charAt(3));
        assertEquals('h',a.charAt(11));
    }
    @Test
    public void testcapacity() throws Exception
    {
        assertEquals(28,a.capacity());
        assertEquals(40,b.capacity());
        assertEquals(52,c.capacity());
    }
    @Test
    public void testlength() throws Exception
    {
        assertEquals(12,a.length());
        assertEquals(24,b.length());
        assertEquals(36,c.length());
    }
    @Test
    public void testindexOf() throws Exception
    {
        assertEquals(0,a.indexOf("Bay"));
        assertEquals(3,a.indexOf("ern"));
        assertEquals(11,b.indexOf("hBa"));
    }
}