package 大二上.第四周.jsjf;


import 大二上.第四周.jsjf.exceptions.*;

/**
 * ArrayUnorderedList represents an array implementation of an unordered list.
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class ArrayUnorderedList<T> extends ArrayList<T> 
         implements UnorderedListADT<T>
{

    public ArrayUnorderedList()
    {
        super();
    }


    public ArrayUnorderedList(int initialCapacity)
    {
        super(initialCapacity);
    }


    @Override
    public void addToFront(T element)
    {
        for (int i = rear; i > -1; i--) {
//                array[index] = element;
            list[i+1] = list[i];
//                array[index+2] = array[index+1];

        }
        list[0] = element;
        rear++;
    }


    @Override
    public void addToRear(T element)
    {
        list[rear] = element;
        rear++;
    }

    /**
     * Adds the specified element after the specified target element.
     * Throws an ElementNotFoundException if the target is not found.
     *
     * @param element the element to be added after the target element
     * @param target  the target that the element is to be added after
     */
    @Override
    public void addAfter(T element, T target)
    {
        if (size() == list.length) {
            expandCapacity();
        }

        int scan = 0;
		
		// find the insertion point
        while (scan < rear && !target.equals(list[scan])) {
            scan++;
        }
      
        if (scan == rear) {
            throw new ElementNotFoundException("UnorderedList");
        }
    
        scan++;
		
		// shift elements up one
        for (int shift=rear; shift > scan; shift--) {
            list[shift] = list[shift-1];
        }

		// insert element
		list[scan] = element;
        rear++;
		modCount++;
    }
}
