package week6;

//*************************************************************************************************
//  Family.java                   Author: zhangyeye233
//
//  Demonstrates the use of variable length parameter lists.
//*************************************************************************************************

public class Family
{
    private String[] menbers;

    //---------------------------------------------------------------------------------------------
    // Construtor: Sets up this family by storing the (possibly multiple) names that are passed are
    // passed in as parameters.
    //---------------------------------------------------------------------------------------------
    public Family(String ... names)
    {
        menbers = names;
    }

    //---------------------------------------------------------------------------------------------
    //  Returns a string representation of this family.
    //---------------------------------------------------------------------------------------------

    @Override
    public String toString() {
        String result = "";

        for (String name : menbers)
            result += name + "\n";

        return result;
    }
}
