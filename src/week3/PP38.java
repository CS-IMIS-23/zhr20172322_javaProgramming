//  This is PP3.8

import java.util.Random;

public class PP38
{
    public static void main(String[] args)
    {
       Random generator = new Random();
       int num1;
       double a, b, c;

       num1 = generator.nextInt(21) + 20;
       System.out.println("The random number: " + num1);
 
       a = Math.cos(num1);
       b = Math.sin(num1);
       c = Math.tan(num1);

       System.out.println("The number sin:" + a);
       System.out.println("The number cos:" + b);
       System.out.println("The number tan:" + c);
    }
}
