package 大二上.第一周;

public class Student2
{
    protected String name;
    protected int number;
    protected String hobby;

    protected Student2 next = null;

    public Student2(String name, int number, String hobby)
    {
        this.name = name;
        this.number = number;
        this.hobby = hobby;
    }
}
