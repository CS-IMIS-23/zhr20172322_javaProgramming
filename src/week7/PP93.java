package week7;

//  This is PP9.3 测试。
public class PP93
{
    public static void main(String[] args) {
        Academic a = new Academic();
        a.names = "一本学术期刊";
        a.About = "他所研究的内容";
        a.mechanism = "他的出版机构";
        System.out.println(a);

        Magazine b = new Magazine();
        b.names = "一本杂志";
        b.JournalNumber = 123456;
        b.Press = "它的出版社";
        b.Keys = "他所想的内容";
        System.out.println(b);

        Novel c = new Novel();
        c.names = "西游记";
        c.About = "一群人和妖怪去西天取经";
        c.AUTHOR = "我忘了";
        System.out.println(c);

        PictureBook d = new PictureBook();
        d.pictureAbout = "儿童连环画";
        d.names = "小屁孩";
        d.AUTHOR = "大屁孩";
        d.Keys = "让小屁孩开心";
        System.out.println(d);

    }
}
