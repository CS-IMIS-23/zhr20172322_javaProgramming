package 大二上.第二周.蓝墨云;

public class NumList {

    //  在结尾插入
    public static void Insert(Num head,Num node){
        Num temp = head;
        while (temp.next != null){
            temp = temp.next;
        }
        temp.next = node;
    }
    // 中间插入（在输入的index之前插入）

    public static void Insert(int index,Num head,Num node){
        int a = 0;
        Num temp = head;
        while (temp != null && a != index - 2){
            a++;
            temp = temp.next;
        }
        node.next = temp.next;
        temp.next = node;
    }

    //  头插法

    public static void preInsert(Num head,Num node)
    {
        Num temp = head.next;
        node.next = temp;
        head.next = node;
        int tem = head.number;
        head.number = node.number;
        node.number = tem;
    }
    //  删除结点
    public static void Delete(int index, Num head){
        Num current = head;
        int a = 0;
        while (current != null && a != index - 2)//中间
        {
            current = current.next;
            a++;
        }
        current.next = current.next.next;
//        else if (current.next.next == null)//尾部
//        {
//            current.next.next = null;
//        }
//        else if (index == 1)//头部
//        {
//            head = head.next;
//            return head;
//        }
//        return current;
    }
    //  输出
    public static void Print(Num head){
        Num node = head;
        while (node != null){
            System.out.print(node.number + " ");
            node = node.next;
        }
    }
    //  使用冒泡排序法进行排序
    public static Num Sort(Num head){
        Num node = head,current = null;

        //  当链表为空或仅有一个结点时
        if (head == null || head.next == null) {
            return head;
        }
        while (node.next != current){
            while (node.next != current){
                if (node.number > node.next.number){
                    int temp = node.number;
                    node.number = node.next.number;
                    node.next.number = temp;
                }
                node = node.next;
            }
            current = node;  //  使下一次遍历的尾结点为当前结点
            node = head;
        }
        return head;
    }
}
