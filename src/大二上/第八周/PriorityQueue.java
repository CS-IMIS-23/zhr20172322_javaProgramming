package 大二上.第八周;

import 大二上.第八周.jsjf.*;

import java.util.function.Consumer;

/**
 * PriorityQueue implements a priority queue using a heap.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class PriorityQueue<T> extends ArrayHeap<PrioritizedObject<T>>
{
    /**
     * Creates an empty priority queue.
     */
    public PriorityQueue() 
    {
        super();
    }

    int count = 0;
    

    public void addElement(T object, int priority) 
    {
        PrioritizedObject<T> obj = new PrioritizedObject<T>(object, priority);
	    super.addElement(obj);
	    count++;
    }

    public T removeNext() 
    {
        PrioritizedObject<T> obj = (PrioritizedObject<T>)super.removeMin();
        return obj.getElement();
    }

    @Override
    public void forEach(Consumer<? super PrioritizedObject<T>> action) {

    }

//    public T getElement()
//    {
//        super.
//    }

    public int length()
    {
        return count;
    }
}


