package 大二上.实验二;

import 大二上.第七周.jsjf.LinkedBinarySearchTree;

public class test5 {
    public static void main(String[] args) {
        LinkedBinarySearchTree a = new LinkedBinarySearchTree(66);

        a.addElement(70);
        a.addElement(77);
        a.addElement(88);
        a.addElement(99);
        a.addElement(33);
        a.addElement(22);
        a.addElement(11);
        a.addElement(55);
        a.addElement(22);
        a.addElement(1);
        a.addElement(33);
        System.out.println("输出整棵树：");
        System.out.println(a.toString());
        System.out.println("-----------------------------------------");
        System.out.println("根结点的右子树的右子树的右子树的右结点：");
        System.out.println(a.getRight().getRight().getRight().getRight());
        System.out.println("-----------------------------------------");
        System.out.println("查找最大值：");
        System.out.println(a.findMax());
        System.out.println("-----------------------------------------");
        System.out.println("查找最小值：");
        System.out.println(a.findMin());
        System.out.println("-----------------------------------------");
        a.removeMax();
        System.out.println("删除最大值之后的最大值：");
        System.out.println(a.findMax());
    }
}
