//***************************************************************************************
//  PianoKeys.kava                     Author: zhangyeye233
//
//  Demonstrates the declaration, initialization, and use of an integer variable.
//***************************************************************************************

public class PianoKeys
{
    //-----------------------------------------------------------------------------------
    //  Prints the number of keys on a piano.
    //-----------------------------------------------------------------------------------
    public static void main(String[] args)
    {
       int keys = 88;
       float a = 5/10;
       System.out.println("A piano has " + keys + " keys."+a);
    }
}
