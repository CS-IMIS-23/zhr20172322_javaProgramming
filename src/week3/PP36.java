package week3;

// This is PP3.6
import java.util.Scanner;

public class PP36
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in) ;
        double a, b, c;

        System.out.println("Please enter the sphere ridius: ");
        a = scan.nextDouble();
        b = 4/3.000000000000000000000*Math.PI*a*a*a;
        c = 4*Math.PI*a*a;

        System.out.println("The sphere volume: " + b );
        System.out.println("The sphere area: " + c );

    }
}
