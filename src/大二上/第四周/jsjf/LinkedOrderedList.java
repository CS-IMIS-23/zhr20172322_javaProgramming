package 大二上.第四周.jsjf;

import 大二上.第四周.jsjf.exceptions.*;

public class LinkedOrderedList<T> extends LinkedList<T>
         implements OrderedListADT<T> {
    /**
     * Creates an empty list.
     */
    public LinkedOrderedList() {
        super();
    }


    @Override
    public void add(T element) {
        if (!(element instanceof Comparable)) {
            throw new NonComparableElementException("LinkedList");
        }
        Comparable<T> comparableElement = (Comparable<T>)element;
        LinearNode node = new LinearNode(comparableElement);
        LinearNode last = head;
        for (int i = 1; i < count; i++) {
            last = last.getNext();
        }
        if (size() == 0) {
            //链为空
            head = node;
            tail = node;
            count++;
            return;
        } else if (comparableElement.compareTo(head.getElement()) < 0) {
            //插入在链首
            node.setNext(head);
            head = node;
            count++;
            return;
        } else if (comparableElement.compareTo((T) last.getElement()) > 0) {
            //插入在链尾
            last.setNext(node);
            count++;
        } else {
            //插入在链中间
            LinearNode current = head;
            LinearNode next = current.getNext();
            for (int i = 0; i < count - 1; i++) {
                if (comparableElement.compareTo((T) next.getElement()) < 0) {
                    current.setNext(node);
                    node.setNext(next);
                    count++;
                    return;
                }
                current = next;
                next = next.getNext();
            }
        }
    }
}





