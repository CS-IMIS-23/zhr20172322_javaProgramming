package week7;

import java.util.Scanner;

public class PP91
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        String answer = "y";
        int a = 0;

        while (answer.equalsIgnoreCase("y"))  //  allows y or Y
        {
            MonetaryCoin b = new MonetaryCoin();
            b.face();
            System.out.println(b);
            a += b.face();

            System.out.println("再来一次？(y/n): ");
            answer = scan.nextLine();
        }
        System.out.println("之前所抛硬币值的和：" + a);
    }
}
