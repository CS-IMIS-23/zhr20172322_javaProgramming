package 大二上.第七周.jsjf;


import 大二上.第四周.jsjf.ListADT;
import 大二上.第四周.jsjf.OrderedListADT;

import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

/**
 * BinarySearchTreeList represents an ordered list implemented using a binary
 * search tree.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class BinarySearchTreeList<T> extends LinkedBinarySearchTree<T>
                      implements ListADT<T>, OrderedListADT<T>, Iterable<T>
{
    /**
     * Creates an empty BinarySearchTreeList.
     */
    public BinarySearchTreeList() 
    {
        super();
    }

    /**
     * Adds the given element to this list.
     *
     * @param element the element to be added to the list
     */
    @Override
    public void add(T element)
    {
        addElement(element);
    }

    /**
     * Removes and returns the first element from this list.
     *
     * @return the first element in the list
     */
    @Override
    public T removeFirst()
    {
        return removeMin();
    }
    
    /**
     * Removes and returns the last element from this list.
     *
     * @return the last element from the list
     */
    @Override
    public T removeLast()
    {
        return removeMax();
    }

   /**
    * Removes and returns the specified element from this list.
    *
    * @param element the element being sought in the list
    * @return the element from the list that matches the target
    */
    @Override
    public T remove(T element)
    {
        return removeElement(element);
    }

   /**
    * Returns a reference to the first element on this list.
    *
    * @return a reference to the first element in the list
    */
    @Override
    public T first()
    {
        return findMin();
    }

   /**
    * Returns a reference to the last element on this list.
    *
    * @return a reference to the last element in the list
    */
    @Override
    public T last()
    {
        return findMax();
    }

    @Override
    public T getRootElement() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean contains(T targetElement) {
        return false;
    }

    @Override
    public String toString() {
        return null;
    }

    /**
    * Returns an iterator for the list.
    *
    * @return an iterator over the elements in the list
    */
    @Override
    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }

    @Override
    public void forEach(Consumer<? super T> action) {

    }

    @Override
    public Spliterator<T> spliterator() {
        return null;
    }

    @Override
    public Iterator<T> iteratorInOrder() {
        return null;
    }

    @Override
    public Iterator<T> iteratorPreOrder() {
        return null;
    }

    @Override
    public Iterator<T> iteratorPostOrder() {
        return null;
    }

    @Override
    public Iterator<T> iteratorLevelOrder() {
        return null;
    }
}

