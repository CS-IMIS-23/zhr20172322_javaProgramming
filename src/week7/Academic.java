package week7;

public class Academic extends ReadingMaterials
{
    String About,mechanism;
    public void setAbout(String ABO)
    {
        About = ABO;
    }

    public String getAbout()
    {
        return About;
    }

    public void setMechanism(String ME)
    {
        mechanism = ME;
    }

    public String getMechanism()
    {
        return mechanism;
    }

    @Override
    public String toString() {
        String a;
        a = "该学术刊物主要关于" + About + "，它的出版机构是" + mechanism + "。";
        return a;
    }
}
