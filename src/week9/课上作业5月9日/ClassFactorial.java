package week9.课上作业5月9日;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class ClassFactorial
{
    public static void main(String[] args)throws FileNotFoundException
    {
        String s;
        PrintStream ps = new PrintStream("Factorial.txt");
        int n;
        Scanner scan = new Scanner(System.in);
        System.out.println("输入一个数n： ");
        n = scan.nextInt();

        int result = fact(n);
        System.out.println("F(n)等于：" + result);

        s = "你输入的n是" + n  + "运算后得到的结果是"+ result;

        // fact(n){
        //  if (n=0) return 0 ;
        //  else if (n=1) return 1;
        //  else return fact(n-1) + fact(n-2)
        ps.append(s);

    }
    static int fact(int n)
    {
        if (n == 0)
            return 0;
        else if (n == 1)
            return 1;
        else
            return fact(n-1) + fact(n-2);
    }
}
