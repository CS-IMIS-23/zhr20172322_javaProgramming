package 实验三;

//   这是对20172303范雯琪同学Complex类的测试，借用了第二次实现对我自己Complex类的测试，进行了一定修改。

import junit.framework.TestCase;
import org.junit.Test;

public class ComplexTest extends TestCase {
    Complex x = new Complex(6, 5);
    Complex y = new Complex(3, 8);


    @Test
    public void testAdd() {                                          //  加法
        assertEquals(9.0, x.complexAdd(y).realPart);
        assertEquals(13.0, x.complexAdd(y).imagePart);
    }

    public void testSub() {                                          //  减法
        assertEquals(3.0, x.complexSub(y).realPart);
        assertEquals(-3.0, x.complexSub(y).imagePart);
    }

    public void testMulti() {                                        //  乘法
        assertEquals(-22.0, x.complexMulti(y).realPart);
        assertEquals(63.0, x.complexMulti(y).imagePart);
    }
}