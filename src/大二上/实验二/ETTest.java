package 大二上.实验二;

import 大二上.第六周.PostfixEvaluator;

import java.util.Scanner;

import static 大二上.实验二.ExpressionTTree.toSuffix;

public class ETTest {
    public static void main(String[] args) {
            String infix, again;
            int result;
            Scanner in = new Scanner(System.in);

            do {
                PostfixEvaluator evaluator = new PostfixEvaluator();

                System.out.println("输入一个中缀表达式 " +
                        " (例如：3 + 6 * 7 / 3 - 2 + 5)");
                System.out.println("这个表达式必须包含int型的数字和操作符(比如：+,-,*,/)");
                //输入中缀表达式
                infix = in.nextLine();

                //使用树将中缀表达式转换为后缀表达式
                String suffix =toSuffix(infix);

                //后缀表达式的计算结果
                result = evaluator.evaluate(suffix);

                System.out.println("\n后缀表达式为：" + suffix);
                System.out.println("计算结果为："+result);

                System.out.println("这个表达树是：:");
                System.out.println(evaluator.getTree());

                System.out.println("再输一个 [Y/N]?");
                again = in.nextLine();
                System.out.println();
            }
            while (again.equalsIgnoreCase("Y"));

        }
    }