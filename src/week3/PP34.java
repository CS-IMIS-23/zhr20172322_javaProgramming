package week3;

import java.util.Scanner;

public class PP34 {
    public static void main(String[] args)
    {
    Scanner scan = new Scanner(System.in);

        double a;
        int b, c;
        System.out.println("Please enter a Floating-point number: ");
        a = scan.nextDouble();

        b = (int)a + 1;
        System.out.println("The smallest integer less than this number is: " + b);
        c = (int)a;
        System.out.println("The biggest integer less than this number is: " + c);
    }
}
