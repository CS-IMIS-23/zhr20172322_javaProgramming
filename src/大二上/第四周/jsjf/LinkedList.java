package 大二上.第四周.jsjf;

import 大二上.第四周.jsjf.exceptions.*;
import java.util.*;

public abstract class LinkedList<T> implements ListADT<T>, Iterable<T>
{
    protected int count;
    protected LinearNode<T>  tail, head;
	protected int modCount;

    public LinkedList()
    {
        count = 0;
        head = tail = null;
		modCount = 0;
	}

    @Override
    public T removeFirst() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        T result;
        LinearNode<T> node = head;
        result = node.getElement();

        head = head.getNext();
        count--;
        return result;

    }

    @Override
    public T removeLast() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> last = head;
        for (int i = 0; i < count-1;i++)
        {
            last = last.getNext();
        }
        count--;
        return last.getElement();

//        T result = head.getElement();
//        head = head.getNext();
//        count--;
//
//        if (isEmpty()) {
//            tail = null;
//        }
//
//        return result;
    }

    @Override
    public T remove(T targetElement) throws EmptyCollectionException,
         ElementNotFoundException 
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
      
        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;
      
        while (current != null && !found) {
            if (targetElement.equals(current.getElement())) {
                found = true;
            } else
            {
                previous = current;
                current = current.getNext();
            }
        }
            
        if (!found) {
            throw new ElementNotFoundException("LinkedList");
        }
      
        if (size() == 1)  // only one element in the list
        {
            head = tail = null;
        } else if (current.equals(head))  // target is at the head
        {
            head = current.getNext();
        } else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        }
        else  // target is in the middle
        {
            previous.setNext(current.getNext());
        }
      
        count--;
		modCount++;
      
        return current.getElement();
    }

    @Override
    public T first() throws EmptyCollectionException
    {

        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        return head.getElement();
    }
	

    @Override
    public T last() throws EmptyCollectionException
    {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> last = head;
        for (int i = 0; i<count-1;i++)
        {
            last = last.getNext();
        }
        return last.getElement();
    }

    @Override
    public boolean contains(T targetElement) throws
         EmptyCollectionException{
    boolean found = false;
    LinearNode<T> previous = null;
    LinearNode<T> current = head;

        while (current != null && !found) {
            if (targetElement.equals(current.getElement())) {
                found = true;
            } else
            {
                previous = current;
                current = current.getNext();
            }
        }

        if (!found)
        {
            return false;
        }
        else {
            return true;
        }
    }


    @Override
    public boolean isEmpty()
    {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size()
    {
        return count;
    }

    @Override
    public String toString()
    {
        LinearNode node = head;
        String result = "";
        int a = count;
        while (a > 0) {
            result +=  node.getElement()+" ";
            node = node.getNext();
            a--;
        }
        return result;
    }

    @Override
    public Iterator<T> iterator()
    {
        return new LinkedListIterator();
    }


	private class LinkedListIterator implements Iterator<T>
	{
		private int iteratorModCount;  // the number of elements in the collection
		private LinearNode<T> current;  // the current position
		

		public LinkedListIterator()
		{
			current = head;
			iteratorModCount = modCount;
		}

		@Override
        public boolean hasNext() throws ConcurrentModificationException
		{
			if (iteratorModCount != modCount) {
                throw new ConcurrentModificationException();
            }
			
			return (current != null);
		}

		@Override
        public T next() throws ConcurrentModificationException
		{
			if (!hasNext()) {
                throw new NoSuchElementException();
            }
			
			T result = current.getElement();
			current = current.getNext();
			return result;
		}
		

		@Override
        public void remove() throws UnsupportedOperationException
		{
			throw new UnsupportedOperationException();
		}
	}
	
}


