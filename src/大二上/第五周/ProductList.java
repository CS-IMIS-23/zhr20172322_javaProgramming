package 大二上.第五周;

import 大二上.第四周.jsjf.ListADT;
import 大二上.第四周.jsjf.exceptions.ElementNotFoundException;
import 大二上.第四周.jsjf.exceptions.EmptyCollectionException;

import java.util.Iterator;

public class ProductList<T> implements ListADT<T> {
    protected int count;
    protected LinearNode<T>head,tail;

    @Override
    public T removeFirst() {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        T result;
        LinearNode<T> node = head;
        result = node.getElement();

        head = head.getNext();
        count--;
        return result;

    }

    @Override
    public T removeLast() {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> last = head;
        for (int i = 0; i < count-1;i++)
        {
            last = last.getNext();
        }
        count--;
        return last.getElement();
    }

    public void add(T product) {
        LinearNode<T> node = new LinearNode<T>(product);

        if (isEmpty()) {
            head = node;
        } else {
            tail.setNext(node);
        }

        tail = node;
        count++;
    }

    @Override
    public T remove(T element) {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        boolean found = false;
        LinearNode<T> previous = null;
        LinearNode<T> current = head;

        while (current != null && !found) {
            if (element.equals(current.getElement())) {
                found = true;
            } else
            {
                previous = current;
                current = current.getNext();
            }
        }

        if (!found) {
            throw new ElementNotFoundException("LinkedList");
        }

        if (size() == 1)  // only one element in the list
        {
            head = tail = null;
        } else if (current.equals(head))  // target is at the head
        {
            head = current.getNext();
        } else if (current.equals(tail))  // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        }
        else  // target is in the middle
        {
            previous.setNext(current.getNext());
        }

        count--;


        return current.getElement();
    }

    @Override
    public T first() {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        return head.getElement();
    }

    @Override
    public T last() {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }

        LinearNode<T> last = head;
        for (int i = 0; i<count-1;i++)
        {
            last = last.getNext();
        }
        return last.getElement();
    }

    @Override
    public boolean contains(T target) {

            boolean found = false;
            LinearNode<T> previous = null;
            LinearNode<T> current = head;

            while (current != null && !found) {
                if (target.equals(current.getElement())) {
                    found = true;
                } else
                {
                    previous = current;
                    current = current.getNext();
                }
            }

            if (!found)
            {
                return false;
            }
            else {
                return true;
            }
    }

    @Override
    public boolean isEmpty() {
        if (size() == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Iterator iterator() {
        return null;
    }


    public int compareTo(大二上.第五周.Product Pr) {
        return 0;
    }

    public void selectionSort() {
        if (isEmpty()) {
            throw new EmptyCollectionException("LinkedList");
        }
        LinearNode<Product> current = (LinearNode<Product>) head;
        LinearNode<Product> temp,temp2,temp3 = null;
        大二上.第五周.Product cod = new 大二上.第五周.Product("123",0,123,123);
        while(current.getNext() != null) {
            temp2 = null;
            temp3 = temp = current;

            while(temp.getNext() != null){
                if(temp.getNext().getElement().getPrice()<temp.getElement().getPrice()){
                    temp3 = temp2 = temp.getNext();
                    cod = (大二上.第五周.Product) temp.getNext().getElement();
                }
                temp = temp.getNext();
            }
            if(temp2 != null){
                temp2.setElement(current.getElement());
                current.setElement( cod);
            }

            current = current.getNext();
        }
    }

    @Override
    public String toString()
    {
        String result = "";
        LinearNode temp;
        temp = head;

        while (temp != null) {
            result += temp.getElement() + "     ";
            temp = temp.getNext();
        }
        return result;
    }
}

